import {browser, by, element} from 'protractor';

//spec.js
describe('Login Scenario', function () {

  it('Login Scenario', function () {
    browser.manage().deleteAllCookies();
    //browser.executeScript('window.sessionStorage.clear(); window.localStorage.clear();');
    browser.manage().window().setSize(1600, 1000);
    browser.get('https://teamillusion100.surge.sh/');
    expect(browser.getTitle()).toEqual('MachiavelliWebClient');
    let btnLogin = element(by.id('gotoLogin'));
    console.log(btnLogin);
    btnLogin.click();
    browser.sleep(1000);
    let username = element(by.id('usernameInput'));
    let pwd = element(by.id('passwordInput'));
    username.sendKeys('abdel');
    pwd.sendKeys('password');
    let btnLoginAndToken = element(by.id('signIn'));
    btnLoginAndToken.click();
    browser.sleep(5000);
    var value = browser.executeScript('return window.localStorage.getItem(\'access_token\');');
    expect(value).not.toBeNull();
  });
});
