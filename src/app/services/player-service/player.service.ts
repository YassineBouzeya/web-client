import { Injectable } from '@angular/core';
import {PlayerDto} from '../../model/PlayerDto';


@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor() {
  }

  player: PlayerDto;
  userName;

  getplayer(): PlayerDto {
    return this.player;
  }

  setplayer(value: PlayerDto) {
    this.player = value;
  }

  getuserName(): string {
    return this.userName;
  }

  setuserName(value: string) {
    this.userName = value;
  }





}


