
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InMemoryCardService {
  createDb() {
    const characters = [
      {
        'id': 1,
        'characterName': 'Moordenaar',
        'imgPath': '~src/assets/Characters/Moordenaar.jpg',
        'extraGoldColor': 5
      },
      {
        'id': 2,
        'characterName': 'Dief',
        'imgPath': '~src/assets/Characters/Dief.jpg',
        'extraGoldColor': 5
      },
      {
        'id': 3,
        'characterName': 'Magier',
        'imgPath': '~src/assets/Characters/Magier.jpg',
        'extraGoldColor': 5
      },
      {
        'id': 4,
        'characterName': 'Koning',
        'imgPath': '~src/assets/Characters/Koning.jpg',
        'extraGoldColor': 0
      },
      {
        'id': 5,
        'characterName': 'Prediker',
        'imgPath': '~src/assets/Characters/Prediker.jpg',
        'extraGoldColor': 1
      },
      {
        'id': 6,
        'characterName': 'Koopman',
        'imgPath': '~src/assets/Characters/Koopman.jpg',
        'extraGoldColor': 2
      },
      {
        'id': 7,
        'characterName': 'Bouwmeester',
        'imgPath': '~src/assets/Characters/Bouwmeester.jpg',
        'extraGoldColor': 5
      }
      ,
      {
        'id': 8,
        'characterName': 'Condottiere',
        'imgPath': '~src/assets/Characters/Condottiere.jpg',
        'extraGoldColor': 3
      }
    ];
    const buildings = [];
    return {characters};
  }

}
