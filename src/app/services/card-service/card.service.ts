import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CharacterDto} from '../../model/CharacterDto';
import {CharacterCardDto} from '../../model/CharacterCardDto';
import {BuildingDto} from '../../model/BuildingDto';
import {BuildingCardDto} from '../../model/BuildingCardDto';
import {GameService} from '../game-service/game.service';
import {PlayerDto} from '../../model/PlayerDto';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  private characterUrl = 'api/characters';
  private characters: CharacterDto[];
  private buildings: BuildingDto[];
  private _card: CharacterCardDto;

  constructor(private http: HttpClient, private gameService: GameService) {
    this.characters = [];
    this.buildings = [];
    this.loadCharacters();
    this.loadBuildings();
  }

  public getCharacters() {
    return this.characters;
  }

  private loadCharacters() {
    const moordenaar = new CharacterDto();
    moordenaar.id = 1;
    moordenaar.characterName = 'Moordenaar';
    moordenaar.imgPath = '../../../../assets/Characters/Moordenaar.jpg';
    moordenaar.extraGoldColor = 5;
    this.characters.push(moordenaar);

    const dief = new CharacterDto();
    dief.id = 2;
    dief.characterName = 'Dief';
    dief.imgPath = '../../../../assets/Characters/Dief.jpg';
    dief.extraGoldColor = 5;
    this.characters.push(dief);

    const magier = new CharacterDto();
    magier.id = 3;
    magier.characterName = 'Magier';
    magier.imgPath = '../../../../assets/Characters/Magier.jpg';
    magier.extraGoldColor = 5;
    this.characters.push(magier);

    const koning = new CharacterDto();
    koning.id = 4;
    koning.characterName = 'Koning';
    koning.imgPath = '../../../../assets/Characters/Koning.jpg';
    koning.extraGoldColor = 0;
    this.characters.push(koning);

    const prediker = new CharacterDto();
    prediker.id = 5;
    prediker.characterName = 'Prediker';
    prediker.imgPath = '../../../../assets/Characters/Prediker.jpg';
    prediker.extraGoldColor = 1;
    this.characters.push(prediker);

    const koopman = new CharacterDto();
    koopman.id = 6;
    koopman.characterName = 'Koopman';
    koopman.imgPath = '../../../../assets/Characters/Koopman.jpg';
    koopman.extraGoldColor = 2;
    this.characters.push(koopman);

    const bouwmeester = new CharacterDto();
    bouwmeester.id = 7;
    bouwmeester.characterName = 'Bouwmeester';
    bouwmeester.imgPath = '../../../../assets/Characters/Bouwmeester.jpg';
    bouwmeester.extraGoldColor = 5;
    this.characters.push(bouwmeester);

    const condottiere = new CharacterDto();
    condottiere.id = 8;
    condottiere.characterName = 'Condottiere';
    condottiere.imgPath = '../../../../assets/Characters/Condottiere.jpg';
    condottiere.extraGoldColor = 3;
    this.characters.push(condottiere);

    const acter = new CharacterDto();
    acter.id = 99;
    acter.characterName = 'Achterkant';
    acter.imgPath = '../../../../assets/Characters/Achterkant.jpg';
    acter.extraGoldColor = 5;
    this.characters.push(acter);
  }

  private loadBuildings() {
    const abdij = new BuildingDto();
    abdij.id = 1;
    abdij.buildingName = 'Abdij';
    abdij.color = 1;
    abdij.cost = 3;
    abdij.imgPath = '../../../../assets/Gebouwen/Abdij.jpg';
    this.buildings.push(abdij);

    const gildenhuis = new BuildingDto();
    gildenhuis.id = 2;
    gildenhuis.buildingName = 'Gildehuis';
    gildenhuis.color = 2;
    gildenhuis.cost = 2;
    gildenhuis.imgPath = '../../../../assets/Gebouwen/Gildehuis.jpg';
    this.buildings.push(gildenhuis);

    const handelshuis = new BuildingDto();
    handelshuis.id = 3;
    handelshuis.buildingName = 'Handelshuis';
    handelshuis.color = 2;
    handelshuis.cost = 3;
    handelshuis.imgPath = '../../../../assets/Gebouwen/Handelshuis.jpg';
    this.buildings.push(handelshuis);

    const haven = new BuildingDto();
    haven.id = 4;
    haven.buildingName = 'Haven';
    haven.color = 2;
    haven.cost = 4;
    haven.imgPath = '../../../../assets/Gebouwen/Haven.jpg';
    this.buildings.push(haven);

    const jachtslot = new BuildingDto();
    jachtslot.id = 5;
    jachtslot.buildingName = 'Jachtslot';
    jachtslot.color = 0;
    jachtslot.cost = 3;
    jachtslot.imgPath = '../../../../assets/Gebouwen/Jachtslot.jpg';
    this.buildings.push(jachtslot);

    const kathedraal = new BuildingDto();
    kathedraal.id = 6;
    kathedraal.buildingName = 'Kathedraal';
    kathedraal.color = 1;
    kathedraal.cost = 5;
    kathedraal.imgPath = '../../../../assets/Gebouwen/Kathedraal.jpg';
    this.buildings.push(kathedraal);

    const kerk = new BuildingDto();
    kerk.id = 7;
    kerk.buildingName = 'Kerk';
    kerk.color = 1;
    kerk.cost = 2;
    kerk.imgPath = '../../../../assets/Gebouwen/Kerk.jpg';
    this.buildings.push(kerk);

    const markt = new BuildingDto();
    markt.id = 8;
    markt.buildingName = 'Markt';
    markt.color = 2;
    markt.cost = 2;
    markt.imgPath = '../../../../assets/Gebouwen/Markt.jpg';
    this.buildings.push(markt);

    const slot = new BuildingDto();
    slot.id = 9;
    slot.buildingName = 'Slot';
    slot.color = 0;
    slot.cost = 4;
    slot.imgPath = '../../../../assets/Gebouwen/Slot.jpg';
    this.buildings.push(slot);

    const paleis = new BuildingDto();
    paleis.id = 10;
    paleis.buildingName = 'Paleis';
    paleis.color = 0;
    paleis.cost = 5;
    paleis.imgPath = '../../../../assets/Gebouwen/Paleis.jpg';
    this.buildings.push(paleis);

    const raadhuis = new BuildingDto();
    raadhuis.id = 11;
    raadhuis.buildingName = 'Raadhuis';
    raadhuis.color = 2;
    raadhuis.cost = 5;
    raadhuis.imgPath = '../../../../assets/Gebouwen/Raadhuis.jpg';
    this.buildings.push(raadhuis);

    const taveerne = new BuildingDto();
    taveerne.id = 12;
    taveerne.buildingName = 'Taveerne';
    taveerne.color = 2;
    taveerne.cost = 1;
    taveerne.imgPath = '../../../../assets/Gebouwen/Taveerne.jpg';
    this.buildings.push(taveerne);

    const tempel = new BuildingDto();
    tempel.id = 13;
    tempel.buildingName = 'Tempel';
    tempel.color = 1;
    tempel.cost = 1;
    tempel.imgPath = '../../../../assets/Gebouwen/Tempel.jpg';
    this.buildings.push(tempel);

    const tornooi = new BuildingDto();
    tornooi.id = 14;
    tornooi.buildingName = 'Toernooiveld';
    tornooi.color = 3;
    tornooi.cost = 3;
    tornooi.imgPath = '../../../../assets/Gebouwen/Toernooiveld.jpg';
    this.buildings.push(tornooi);

    const vesting = new BuildingDto();
    vesting.id = 15;
    vesting.buildingName = 'Vestiging';
    vesting.color = 3;
    vesting.cost = 5;
    vesting.imgPath = '../../../../assets/Gebouwen/Vestiging.jpg';
    this.buildings.push(vesting);

    const wacht = new BuildingDto();
    wacht.id = 16;
    wacht.buildingName = 'Wachttoren';
    wacht.color = 3;
    wacht.cost = 1;
    wacht.imgPath = '../../../../assets/Gebouwen/Wachttoren.jpg';
    this.buildings.push(wacht);


    const kerker = new BuildingDto();
    kerker.id = 17;
    kerker.buildingName = 'Kerker';
    kerker.color = 3;
    kerker.cost = 2;
    kerker.imgPath = '../../../../assets/Gebouwen/Kerker.jpg';
    this.buildings.push(kerker);

    const achter = new BuildingDto();
    achter.id = 99;
    achter.buildingName = 'Achterkant';
    achter.color = 0;
    achter.cost = 0;
    achter.imgPath = '../../../../assets/Gebouwen/Achterkant.jpg';
    this.buildings.push(achter);
  }

  public getCharacter(id: number): CharacterDto {
    return this.characters.find(character => character.id === id);
  }

  public getBuilding(id: number): BuildingDto {
    return this.buildings.find(building => building.id === id);
  }


  get card(): CharacterCardDto {
    return this._card;
  }

  set card(value: CharacterCardDto) {
    this._card = value;
  }

  get topTwoBuildings(): BuildingCardDto[] {
    let game = this.gameService.getGame();
    let myList = [];
    let b1 = game.buildingCards.pop();
    myList.push(b1);
    let b2 = game.buildingCards.pop();
    myList.push(b2);
    this.gameService.setGame(game);
    return myList;
  }


  returnBuildingToStack(building: BuildingCardDto) {
    let game = this.gameService.getGame();
    game.buildingCards.push(building);
    this.gameService.setGame(game);
  }

  addBuildingCardToPlayer(building: BuildingCardDto, userName: String) {
    let game = this.gameService.getGame();
    game.players.forEach(function (value) {
      if (value.userName === userName) {
        value.buildingCards.push(building);
      }
    });
    this.gameService.setGame(game);
  }

  discardTheBuilding(building: BuildingCardDto) {
    let game = this.gameService.getGame();
    building.discarded = true;
    this.gameService.setGame(game);
  }

  buyBuilding(building: BuildingCardDto, userName: String) {
    let game = this.gameService.getGame();
    let player = game.players.find(value => value.userName === userName);
    let cost = building.building.cost;
    let money = player.nrOfCoins;
    let buildingBought = false;
    player.buildingCards.forEach(value => {
      if (value.id === building.id && !value.bought) {
        if (money >= cost) {
          value.bought = true;
          buildingBought = true;
        } else {
          //Not enough coins to destroy, could show some feedback
        }
      }
    });
    if (buildingBought) {
      player.nrOfCoins = player.nrOfCoins - cost;
      player = this.checkFirstWithReqBuildings(game, player);
      game.players.forEach(function (value) {
        if (value.userName === userName) {
          value = player;
        }
      });
    }
    this.gameService.setGame(game);
  }

  checkFirstWithReqBuildings(game, player: PlayerDto) {
    if (player.buildingCards.filter(b => b.bought).length >= game.reqBuildings) {
      if (game.players.filter(p => p.firstWithReqBuildings).length === 0) {
        player.firstWithReqBuildings = true;
      }
    }
    return player;
  }
}
