import { Injectable } from '@angular/core';

// voor service moet je @Injectable doen
@Injectable({
  providedIn: 'root'
})
export class LoggingService {

  logstatusChange(status: string) {
    console.log('A server status changed, new status: ' + status);
  }

}
