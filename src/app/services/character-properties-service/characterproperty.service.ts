import {Injectable} from '@angular/core';
import {BuildingCardDto} from '../../model/BuildingCardDto';
import {GameService} from '../game-service/game.service';
import {PlayerService} from '../player-service/player.service';
import {Globals} from '../../components/globals';
import {CardService} from '../card-service/card.service';


@Injectable({
  providedIn: 'root'
})
export class CharacterpropertyService {

  constructor(private playerService: PlayerService,
              private gameService: GameService,
              private globals: Globals,
              private cardService: CardService) {
  }

  game = this.gameService.getGame();
  userName = localStorage.getItem('USERNAME');
  player = this.playerService.getplayer();

  condotiareRemoveBuilding(buildingcard: BuildingCardDto, usernameOfRival: String) {
    let playerThatGetsBuildingDestroyed = this.game.players.find(value => value.userName === usernameOfRival);
    if (playerThatGetsBuildingDestroyed.characterCard.character.id !== 5) {
      let checkIfPossible = this.removeCoinsAndCheck(buildingcard);
      if (checkIfPossible) {
        let coins = this.player.nrOfCoins;
        let cost = buildingcard.building.cost;
        this.game.players.find(value => value.userName === this.userName).nrOfCoins = coins - (cost - 1);
      }
    }
  }

  removeCoinsAndCheck(buildingCard: BuildingCardDto) {
    if (this.player.characterCard.character.id === 8) {
      let coins = this.player.nrOfCoins;
      let cost = buildingCard.building.cost;
      return coins >= cost;
    } else {
      return false;
    }
  }

  magicianStealBuildingCards(userName: String) {
    const _this = this;
    let game = this.gameService.getGame();
    let stealFrom = game.players.find(value => value.userName === userName);
    let myself = game.players.find(value => value.userName === _this.userName);
    let myHandBuildings = myself.buildingCards.filter(value => !value.bought);
    let otherPlayersHandBuildings = stealFrom.buildingCards.filter(value => !value.bought);
    let myBoughtBuildings = myself.buildingCards.filter(value => value.bought);
    let otherPlayersBoughtBuildings = stealFrom.buildingCards.filter(value => value.bought);
    stealFrom.buildingCards = [];
    myself.buildingCards = [];
    myBoughtBuildings.forEach(value => myself.buildingCards.push(value));
    otherPlayersHandBuildings.forEach(value => myself.buildingCards.push(value));
    otherPlayersBoughtBuildings.forEach(value => stealFrom.buildingCards.push(value));
    myHandBuildings.forEach(value => stealFrom.buildingCards.push(value));
    game.players.forEach(value => {
      if (value.userName === myself.userName) {
        value.buildingCards = myself.buildingCards;
      }
      if (value.userName === stealFrom.userName) {
        value.buildingCards = stealFrom.buildingCards;
      }
    });
    this.gameService.setGame(game);
  }

  killCharacter(characterName: String) {
    //this method represents the Killer character property
    let game = this.gameService.getGame();
    game.players.forEach(value => {
      if (value.characterCard.character.characterName === characterName) {
        value.characterCard.killed = true;
      }
    });
    this.gameService.setGame(game);
  }

  stealFrom(characterName: String) {
    let game = this.gameService.getGame();
    game.players.forEach(value => {
      if (value.characterCard.character.characterName === characterName) {
        value.characterCard.robbed = true;
      }
    });
    this.gameService.setGame(game);
  }

  kingReceiveCoins() {
    const _this = this;
    let game = this.gameService.getGame();
    let extraCoins = 0;
    let player = game.players.find(value => value.userName === _this.userName);
    player.buildingCards.forEach(value => {
      if (value.bought && value.building.color === 0) {
        extraCoins = extraCoins + 1;
      }
    });
    player.nrOfCoins = player.nrOfCoins + extraCoins;
    this.playerService.setplayer(player);
    game.players.forEach(value => {
      if (value.userName === _this.userName) {
        value = _this.player;
      }
    });
    this.gameService.setGame(game);
  }

  merchantReceiveCoins() {
    const _this = this;
    let game = this.gameService.getGame();
    let extraCoins = 1;
    let player = game.players.find(value => value.userName === _this.userName);
    player.buildingCards.forEach(value => {
      if (value.bought && value.building.color === 2) {
        extraCoins = extraCoins + 1;
      }
    });
    player.nrOfCoins = player.nrOfCoins + extraCoins;
    this.playerService.setplayer(player);
    game.players.forEach(value => {
      if (value.userName === _this.userName) {
        value = _this.player;
      }
    });
    this.gameService.setGame(game);
  }

  bishopReceiveCoins() {
    const _this = this;
    let game = this.gameService.getGame();
    let extraCoins = 0;
    let player = game.players.find(value => value.userName === _this.userName);
    player.buildingCards.forEach(value => {
      if (value.bought && value.building.color === 1) {
        extraCoins = extraCoins + 1;
      }
    });
    player.nrOfCoins = player.nrOfCoins + extraCoins;
    this.playerService.setplayer(player);
    game.players.forEach(value => {
      if (value.userName === _this.userName) {
        value = _this.player;
      }
    });
    this.gameService.setGame(game);
  }

  architectReceiveBuildings() {
    const _this = this;
    let extraBuildings = this.cardService.topTwoBuildings;
    extraBuildings.forEach(value => {
      _this.cardService.addBuildingCardToPlayer(value, _this.userName);
    });
  }

  warLordReceiveCoins() {
    const _this = this;
    let game = this.gameService.getGame();
    let extraCoins = 0;
    let player = game.players.find(value => value.userName === _this.userName);
    player.buildingCards.forEach(value => {
      if (value.bought && value.building.color === 3) {
        extraCoins = extraCoins + 1;
      }
    });
    player.nrOfCoins = player.nrOfCoins + extraCoins;
    this.playerService.setplayer(player);
    game.players.forEach(value => {
      if (value.userName === _this.userName) {
        value = _this.player;
      }
    });
    this.gameService.setGame(game);
  }

  warLordRemoveBuilding(building: BuildingCardDto) {
    let game = this.gameService.getGame();
    let rivalUserName: String;
    game.players.forEach(value => {
      value.buildingCards.forEach(value1 => {
        if (value1.id === building.id) {
          rivalUserName = value.userName;
        }
      });
    });
    let player = game.players.find(value => value.userName === rivalUserName);

    if (building.building.cost === 1) {
      player.buildingCards.splice(player.buildingCards.findIndex(e => e.id === building.id), 1);
    } else {
      if (this.checkIfPossible(building)) {
        player.buildingCards.splice(player.buildingCards.findIndex(e => e.id === building.id), 1);
        game.players.find(p => p.userName === this.userName).nrOfCoins -= building.building.cost - 1;
      } else {
      }
    }
    this.gameService.setGame(game);
  }

  checkIfPossible(building: BuildingCardDto) {
    let myself = this.playerService.getplayer();
    return myself.nrOfCoins >= building.building.cost;
  }
}
