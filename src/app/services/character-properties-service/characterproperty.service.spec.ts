import {async, ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {CharacterpropertyService} from './characterproperty.service';
import {ModalCharacterPropertyComponent} from '../../components/game/game-board/modal-character-property/modal-character-property.component';
import {AppModule} from '../../components/app.module';
import {AuthenticationService} from '../security/authentication.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {notEqual} from 'assert';


describe('CharacterpropertyService', () => {
  let component: ModalCharacterPropertyComponent;
  let service: CharacterpropertyService;
  let fixture: ComponentFixture<ModalCharacterPropertyComponent>;
  let authService: AuthenticationService;


  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
     // declarations: [ ModalCharacterPropertyComponent ],
      imports: [AppModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
   fixture = TestBed.createComponent(ModalCharacterPropertyComponent);
   component = fixture.componentInstance;
   fixture.detectChanges();
   service = TestBed.get(CharacterpropertyService);
   authService = TestBed.get(AuthenticationService);
   component.ngOnInit();
  });

 //it('calling authService from service when login is called', () => {
 //  let insertSpy = spyOn(authService, 'login').and.callThrough();
 //  expect(insertSpy).toHaveBeenCalled();
 //});

  it('magicianStealBuildingCards should be called', () => {
    let insertSpy = spyOn(service, 'magicianStealBuildingCards').and.callThrough();
    component.magicianStealBuildingCards("iets");
    expect(insertSpy).toHaveBeenCalled();
  });

  it('magicianStealBuildingCards should be called', () => {
    let insertSpy = spyOn(service, 'magicianStealBuildingCards').and.callThrough();
    let before = component.game;
    component.magicianStealBuildingCards("admin.admin");
    let after = component.game;
    expect(insertSpy).toHaveBeenCalled();
    expect(before).not.toEqual(after);
  });

  it('game should be called and be defined', () => {
    let insertSpy = spyOn(service, 'game').and.callThrough();
    expect(component.game).toBeDefined();
  });

  it('architectReceiveBuildings should be called', () => {
    let insertSpy = spyOn(service, 'architectReceiveBuildings').and.callThrough();
    component.architectReceiveBuildings();
    expect(insertSpy).toHaveBeenCalled();
  });

  it('bishopReceiveCoins should be called', () => {
    let insertSpy = spyOn(service, 'bishopReceiveCoins').and.callThrough();
    component.bishopReceiveCoins();
    expect(insertSpy).toHaveBeenCalled();
  });

  it('condotiareRemoveBuilding should be called', () => {
    let insertSpy = spyOn(service, 'condotiareRemoveBuilding').and.callThrough();
    //component.
    expect(insertSpy).toHaveBeenCalled();
  });

  it('killCharacter should be called', () => {
    let insertSpy = spyOn(service, 'killCharacter').and.callThrough();
    component.killCharacter("dief");
    expect(insertSpy).toHaveBeenCalled();
  });

  it('kingReceiveCoins should be called', () => {
    let insertSpy = spyOn(service, 'kingReceiveCoins').and.callThrough();
    component.kingReceiveCoins();
    expect(insertSpy).toHaveBeenCalled();
  });

  it('merchantReceiveCoins should be called', () => {
    let insertSpy = spyOn(service, 'merchantReceiveCoins').and.callThrough();
    expect(insertSpy).toHaveBeenCalled();
  });

  it('removeCoinsAndCheck should be called', () => {
    let insertSpy = spyOn(service, 'removeCoinsAndCheck').and.callThrough();
    expect(insertSpy).toHaveBeenCalled();
  });



});

//describe("CharacterService will be tested", function() {
//  it("dummyTest", function() {
//    expect(true).toBe(service.testMeAndExpectTrue());
//  });
//});
