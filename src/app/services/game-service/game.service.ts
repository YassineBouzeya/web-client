import {Injectable} from '@angular/core';
import {GameDto} from '../../model/GameDto';
import {PlayerDto} from '../../model/PlayerDto';
import {AndroidNotificationDto} from '../../model/AndroidNotificationDto';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../../components/globals';
import {NotificationsService} from '../notification-service/notifications.service';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  game: GameDto;
  users = [];
  players: PlayerDto[] = [];
  private _androidConnected = false;
  roundChosing: boolean;

  constructor(private http: HttpClient, private globals: Globals, private notificationsService: NotificationsService) {
  }

  getGame() {
    return this.game;
  }

  setGame(game: GameDto) {
    this.game = game;
  }

  getUsers() {
    return this.users;
  }

  setUsers(users: []) {
    this.users = users;
  }

  setPlayers(players: PlayerDto[]) {
    this.players = players;
  }

  getPlayers() {
    return this.players;
  }


  get androidConnected(): boolean {
    return this._androidConnected;
  }

  set androidConnected(value: boolean) {
    this._androidConnected = value;
  }

  addCoinsToPlayer(userName: String) {
    let game = this.getGame();
    game.players.forEach(function (value) {
      if (value.userName === userName) {
        value.nrOfCoins = value.nrOfCoins + 2;
      }
    });
    game.coinsLeft = game.coinsLeft - 2;
    game.playerTurn = game.playerTurn + 1;
    if (game.playerTurn >= game.players.length) {
      game.playerTurn = 0;
    }
    this.setGame(game);
  }

  getRoundChosing() {
    return this.roundChosing;
  }

  setRoundChosing(value: boolean) {
    this.roundChosing = value;
  }

  sendAndroidNotification(androidNotificationDto: AndroidNotificationDto) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post<AndroidNotificationDto>(this.globals.backendUrl + '/sendAndroidNotification', androidNotificationDto, httpOptions)
      .subscribe();
  }

  gameOnDestroy(userSettings, gameId) {
    if (this.androidConnected) {
      this.globals.stompClient.unsubscribe('androidChooseCharacter' + gameId);
      this.globals.stompClient.unsubscribe('androidCoinsClick' + gameId);
      this.globals.stompClient.unsubscribe('androidCardsClick' + gameId);
      this.globals.stompClient.unsubscribe('androidBuildingSelected' + gameId);
    }
    this.globals.stompClient.unsubscribe('change' + gameId);
    this.globals.stompClient.unsubscribe('refresh' + gameId);
    this.globals.stompClient.unsubscribe('androidConnect' + gameId);
    if (userSettings.myTurnNotifications) {
      this.notificationsService.subMyTurnNotif();
    }
    if (userSettings.otherTurnNotifications) {
      this.notificationsService.subOtherTurnNotif();
    }
  }
}
