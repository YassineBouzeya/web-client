import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

import {TOKEN_AUTH_PASSWORD, TOKEN_AUTH_USERNAME} from './auth.constant';
import {Globals} from '../../components/globals';


//   service that sends authentication request to SpringBoot to get JWT token.
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  static AUTH_TOKEN = '/oauth/token';


  constructor(private http: Http, private globals: Globals) {
  }


    login(username: string, password: string) {
    const body = `username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}&grant_type=password`;

    // Create the header
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Basic ' + btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD)); // jwtclientid:XY7kmzoNzl100
    // Do post with token and header
    return this.http.post(this.globals.backendUrl + AuthenticationService.AUTH_TOKEN, body, {headers})
      .map(res => res.json())
      .map((res: any) => {
        if (res.access_token) {
          return res.access_token;
        }
        return null;
      });

  }
}
