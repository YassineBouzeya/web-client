import { TestBed } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {UserService} from './user.service';
import * as Http from 'http';
import {HttpClient} from '@angular/common/http';

describe('AuthenticationService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    imports: [UserService],
    schemas: [NO_ERRORS_SCHEMA]
    }),
  );

  it('#getValue should return real value', () => {
    let service: AuthenticationService;
    expect(service.login('yassine.bouzeya', 'jwtpass')).toBeTruthy();


    // create `getValue` spy on an object representing the ValueService
    const valueServiceSpy =
      jasmine.createSpyObj('AuthenticationService', ['login']);
  });
 it('should be created', () => {
   const service: AuthenticationService = TestBed.get(AuthenticationService);
   expect(service).toBeTruthy();
 });


});
