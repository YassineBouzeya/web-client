import {Injectable} from '@angular/core';
import {NotificationboxComponent} from '../../components/notificationbox/notificationbox.component';
import {GameDto} from '../../model/GameDto';
import {Globals} from '../../components/globals';
import {JsonConvert} from 'json2typescript';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  jsonConvert: JsonConvert = new JsonConvert();

  constructor(private globals: Globals) {
  }

  subMyTurnNotif() {
    const _this = this;
    this.globals.stompClient.subscribe('/user/r/myTurn', function (game) {
      const jsonObj: object = JSON.parse(game.body);
      NotificationboxComponent.myTurnGame = _this.jsonConvert.deserializeObject(jsonObj, GameDto);
      $('#friendRequest').hide();
      $('#gameInvite').hide();
      $('#otherTurn').hide();
      $('#myTurn').show();
    }, {id: 'myTurnNotif'});
  }

  subOtherTurnNotif() {
    const _this = this;
    this.globals.stompClient.subscribe('/user/r/myTurn', function (game) {
      const jsonObj: object = JSON.parse(game.body);
      NotificationboxComponent.myTurnGame = _this.jsonConvert.deserializeObject(jsonObj, GameDto);
      $('#friendRequest').hide();
      $('#gameInvite').hide();
      $('#myTurn').hide();
      $('#otherTurn').show();
    }, {id: 'otherTurnNotif'});
  }
}


