import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('AndroidNotificationDto')
export class AndroidNotificationDto {
  @JsonProperty('username', String)
  username: String = undefined;
  @JsonProperty('gameId', Number)
  gameId: number = undefined;
  @JsonProperty('gameTitle', String)
  gameTitle: String = undefined;
  @JsonProperty('content', String)
  content: String = undefined;
}
