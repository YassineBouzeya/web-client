import {JsonObject, JsonProperty} from 'json2typescript';
import {BuildingDto} from './BuildingDto';

@JsonObject('BuildingCardDto')
export class BuildingCardDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('building', BuildingDto)
  building: BuildingDto = undefined;
  @JsonProperty('bought', Boolean)
  bought: boolean = undefined;
  @JsonProperty('discarded', Boolean)
  discarded: boolean = undefined;
}
// The booleans are not prefixed with 'is' because of deserialization bug, removing prefix seems to be a working fix.
