import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('UserDto')
export class UserDto {
  @JsonProperty('firstName', String)
  firstName: String = undefined;
  @JsonProperty('lastName', String)
  lastName: String = undefined;
  @JsonProperty('email', String)
  email: String = undefined;
  @JsonProperty('username', String)
  username: String = undefined;
  @JsonProperty('newUsername', String)
  newUsername: String = undefined;
  @JsonProperty('password', String)
  password: String = undefined;
  @JsonProperty('birthday', Date)
  birthday: Date = undefined;
  @JsonProperty('avatar', String)
  avatar: String = undefined;
}

