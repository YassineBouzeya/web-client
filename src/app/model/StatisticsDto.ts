import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('StatisticsDto')
export class StatisticsDto {
  @JsonProperty('numberOfGames', Number)
  numberOfGames: number = undefined;
  @JsonProperty('numberOfWins', Number)
  numberOfWins: number = undefined;
  @JsonProperty('averageScore', Number)
  averageScore: number = undefined;
  @JsonProperty('topScore', Number)
  topScore: number = undefined;
}
