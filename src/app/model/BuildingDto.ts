import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('BuildingDto')
export class BuildingDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('buildingName', String)
  buildingName: String = undefined;
  @JsonProperty('imgPath', String)
  imgPath: String = undefined;
  @JsonProperty('color', Number)
  color: number = undefined;
  @JsonProperty('cost', Number)
  cost: number = undefined;
}
