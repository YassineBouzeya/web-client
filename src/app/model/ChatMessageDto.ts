import {JsonObject, JsonProperty} from 'json2typescript';
import {GameDto} from './GameDto';

@JsonObject('ChatMessageDto')
export class ChatMessageDto {
  @JsonProperty('game', GameDto)
  game: GameDto = undefined;
  @JsonProperty('username', String)
  username: String = undefined;
  @JsonProperty('timestamp', String)
  timestamp: String = undefined;
  @JsonProperty('content', String)
  content: String = undefined;
  @JsonProperty('avatar', String)
  avatar: String = undefined;
}
