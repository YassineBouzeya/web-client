import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('PrivacyDto')
export class PrivacyDto {
  @JsonProperty('username', String)
  username: String = undefined;
  @JsonProperty('public', Boolean)
  public: boolean = undefined;
}
