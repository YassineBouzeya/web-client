import {JsonObject, JsonProperty} from 'json2typescript';
import {GameDto} from './GameDto';

@JsonObject('changeCaracterDto')
export class ChangeCaracterDto {
  @JsonProperty('gameId', Number)
  gameId: number = undefined;
  @JsonProperty('playerId', Number)
  playerId: number = undefined;
  @JsonProperty('characterCardId', Number)
  characterCardId: number = undefined;
  @JsonProperty('gameDto', GameDto)
  gameDto: GameDto = undefined;

}
