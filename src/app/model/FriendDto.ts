import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('FriendDto')
export class FriendDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('userName', String)
  userName: String = undefined;
  @JsonProperty('email', String)
  email: string = undefined;
}
