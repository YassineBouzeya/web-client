import {JsonObject, JsonProperty} from 'json2typescript';

@JsonObject('SearchUsersDto')
export class SearchUsersDto {
  @JsonProperty('username', String)
  username: String = undefined;
}
