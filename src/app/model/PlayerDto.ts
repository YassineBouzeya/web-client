import {JsonObject, JsonProperty} from 'json2typescript';
import {UserDto} from './UserDto';
import {CharacterCardDto} from './CharacterCardDto';
import {BuildingCardDto} from './BuildingCardDto';

@JsonObject('PlayerDto')
export class PlayerDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('userName', String)
  userName: String = undefined;
  @JsonProperty('avatar', String)
  avatar: String = undefined;
  @JsonProperty('users', [UserDto])
  users: UserDto[] = undefined;
  @JsonProperty('characterCard', CharacterCardDto)
  characterCard: CharacterCardDto = undefined;
  @JsonProperty('buildingCards', [BuildingCardDto])
  buildingCards: BuildingCardDto[] = undefined;
  @JsonProperty('hasCrown', Boolean)
  hasCrown: boolean = undefined;
  @JsonProperty('orderNr', Number)
  orderNr: number = undefined;
  @JsonProperty('nrOfCoins', Number)
  nrOfCoins: number = undefined;
  @JsonProperty('endGamePoints', Number)
  endGamePoints: number = undefined;
  @JsonProperty('host', Boolean)
  host: boolean = undefined;
  @JsonProperty('firstWithReqBuildings', Boolean)
  firstWithReqBuildings: boolean = undefined;
  handBuildingCards: BuildingCardDto[] = [];
  boughtBuildingCards: BuildingCardDto[] = [];

}

// The booleans are not prefixed with 'is' because of deserialization bug, removing prefix seems to be a working fix.
