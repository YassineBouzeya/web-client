import {JsonObject, JsonProperty} from 'json2typescript';
import {UserDto} from './UserDto';

@JsonObject('FriendRequestDto')
export class FriendRequestDto {
  @JsonProperty('id', Number)
  id: number = undefined;
  @JsonProperty('sender', String)
  sender: String = undefined;
  @JsonProperty('receiver', String)
  receiver: String = undefined;
  @JsonProperty('timestamp', String)
  timestamp: String = undefined;

  parsedTimestamp: String = undefined;
}
