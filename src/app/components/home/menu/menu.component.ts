import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '../../globals';
import * as $ from 'jquery';
import {FriendRequestDto} from '../../../model/FriendRequestDto';
import {HttpClient} from '@angular/common/http';
import {HeaderComponent} from '../../header/header.component';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  public isVisible = false;
  friendRequestCount;
  headerComponent = HeaderComponent;

  constructor(private route: ActivatedRoute, private router: Router, private globals: Globals, private http: HttpClient) {
  }

  ngOnInit() {
    this.isAuthenticated();
  }

  gotoHostSettings() {
    this.router.navigate(['/hostSettings']);
  }

  gotoSearchGames() {
    this.router.navigate(['/searchGames']);
  }

  gotoGameOverView() {
    this.router.navigate(['/gameOverview']);
  }

  gotoFriends() {
    this.router.navigate(['/friends']);
  }

  gotoSearchUsers() {
    this.router.navigate(['/searchUsers']);
  }

  gotoAbout() {
    this.router.navigate(['/about']);
  }

  gotoRules() {
    this.router.navigate(['/rules']);
  }

  isAuthenticated() {
    const _this = this;
    const accestoken = localStorage.getItem('access_token');
    if (accestoken == null) {
      this.isVisible = true;
    } else {
      this.isVisible = false;
      this.http.get<FriendRequestDto[]>(this.globals.backendUrl + '/getReceivedFriendRequests').subscribe(data => this.friendRequestCount = data.length, null, function () {
        _this.globals.initializeWebSocketConnection(function () {
          _this.globals.stompClient.subscribe('/user/r/friendRequest', function () {
            _this.friendRequestCount += 1;
            $('#friendBadge').text(_this.friendRequestCount);
          }, {id: 'receivedFriendRequests'});
        });
      });
    }
  }
}
