import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameScoreScreenComponent } from './game-score-screen.component';

describe('GameScoreScreenComponent', () => {
  let component: GameScoreScreenComponent;
  let fixture: ComponentFixture<GameScoreScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameScoreScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameScoreScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
