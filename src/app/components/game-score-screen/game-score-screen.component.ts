import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../globals';
import {GameDto} from '../../model/GameDto';
import {GameService} from '../../services/game-service/game.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-game-score-screen',
  templateUrl: './game-score-screen.component.html',
  styleUrls: ['./game-score-screen.component.css']
})
export class GameScoreScreenComponent implements OnInit {
  game: GameDto;
  gameId;

  constructor(private http: HttpClient, private globals: Globals, private gameSerivce: GameService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const _this = this;
    this.route.params.subscribe(params =>
      this.gameId = params['id']
    );
    this.http.get<GameDto>(this.globals.backendUrl + '/getGame/' + this.gameId).subscribe(data => this.game = data, null, function () {
      _this.game.players.sort(p => p.endGamePoints).reverse();
    });
  }
}
