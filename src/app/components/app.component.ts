import {Component, NgModule} from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {NotificationboxComponent} from './notificationbox/notificationbox.component';
import {MenuComponent} from './home/menu/menu.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@NgModule({
  imports: []
})

export class AppComponent {
  headerComponent = HeaderComponent;

  constructor() {

  }
}
