import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AvatarDialogData} from '../profile.component';

@Component({
  selector: 'app-avatardialog',
  templateUrl: './avatar-dialog.component.html',
  styleUrls: ['./avatar-dialog.component.css']
})
export class AvatarDialogComponent {
  listAvatars: string[] = [
    'avatar1',
    'avatar2',
    'avatar3',
    'avatar4',
    'avatar5',
    'avatar6',
    'avatar7',
    'avatar8',
    'avatar9',
    'avatar10'
  ];

  constructor(public dialogRef: MatDialogRef<AvatarDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: AvatarDialogData) { }

  selectAvatar(selectedAvatar): void {
    this.data.avatar = selectedAvatar;
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }
}
