import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../services/security/user.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {InviteDto} from '../../model/InviteDto';
import {UserDto} from '../../model/UserDto';
import * as $ from 'jquery';
import {Globals} from '../globals';
import {StatisticsDto} from '../../model/StatisticsDto';
import {MatDialog} from '@angular/material';
import {AvatarDialogComponent} from './avatardialog/avatar-dialog.component';

export interface AvatarDialogData {
  avatar: string;
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: UserDto;
  newUser: UserDto;
  profileForm: FormGroup;
  username: FormControl;
  email: FormControl;
  firstname: FormControl;
  lastname: FormControl;
  password: FormControl;
  repeatPassword: FormControl;
  newAvatar: String;

  showStatistics: boolean;
  statistics: StatisticsDto;
  numberOfGames: number;
  numberOfWins: number;
  averageScore: number;
  topScore: number;

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private http: HttpClient,
              private globals: Globals, public dialog: MatDialog) {
    this.showStatistics = false;
    this.statistics = new StatisticsDto();
  }

  ngOnInit() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    const _this = this;
    // this.http.get<UserDto>(this.globals.backendUrl + '/getUser', httpOptions).subscribe(data => this.user = data, null);
    this.http.get<UserDto>(this.globals.backendUrl + '/getUser', httpOptions).subscribe(function(data) {
      _this.user = data;
      _this.setupForms(data);
    }, function () {
      _this.setupForms(null);
    });

    this.http.get<StatisticsDto>(this.globals.backendUrl + '/getStatistics/' + localStorage.getItem('USERNAME'), httpOptions)
      .subscribe(function (data) {
        _this.statistics = data;
      });

    this.username = new FormControl('', Validators.required);
    this.email = new FormControl('', Validators.required);
    this.firstname = new FormControl('', Validators.required);
    this.lastname = new FormControl('', Validators.required);
    this.password = new FormControl('', Validators.required);
    this.repeatPassword = new FormControl('', Validators.required);
    this.profileForm = new FormGroup(
      {
        username: this.username,
        email: this.email,
        firstname: this.firstname,
        lastname: this.lastname,
        password: this.password,
        repeat_password: this.repeatPassword
      }
    );
  }

  setupForms(userDto: UserDto) {
    if (userDto != null) {
      this.username = new FormControl(userDto.username, Validators.required);
      this.email = new FormControl(userDto.email, Validators.required);
      this.firstname = new FormControl(userDto.firstName, Validators.required);
      this.lastname = new FormControl(userDto.lastName, Validators.required);
      this.password = new FormControl('');
      this.repeatPassword = new FormControl('');
      this.newAvatar = userDto.avatar;
      this.profileForm = new FormGroup(
        {
          username: this.username,
          email: this.email,
          firstname: this.firstname,
          lastname: this.lastname,
          password: this.password,
          repeat_password: this.repeatPassword
        }
      );
    }
  }

  switchView(text) {
    if (text === 'Mijn statistieken' && !this.showStatistics) {
      this.showStatistics = true;
    } else if (text === 'Mijn profiel' && this.showStatistics) {
      this.showStatistics = false;
    }
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(AvatarDialogComponent, {
      width: '500px',
      data: { avatar: this.user.avatar }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null) {
        this.newAvatar = result;
      } else {
        this.newAvatar = this.user.avatar;
      }
    });
  }

  saveUser() {
    const httpOptions = {
      headers: new HttpHeaders( {
          'Content-Type': 'application/json'
        })
    };

    if (this.profileForm.valid) {
      this.newUser = new UserDto();
      this.newUser.firstName = this.firstname.value;
      this.newUser.lastName = this.lastname.value;
      this.newUser.email = this.email.value;
      this.newUser.username = this.user.username;
      this.newUser.newUsername = this.username.value;
      this.newUser.password = this.password.value;
      this.newUser.birthday = this.user.birthday;
      this.newUser.avatar = this.newAvatar;
    }

    this.http.post<UserDto>(this.globals.backendUrl + '/saveUser', this.newUser, httpOptions).subscribe(function () {
    });

    document.getElementById('meldingsucces').style.display = 'block';
    this.hideImage();

  }


   doHide(){
    document.getElementById( "meldingsucces" ).style.display = "none" ;
  }

   hideImage(){
    //  5000 = 5 seconds
    setTimeout( "doHide()", 3000 ) ;
  }
}
