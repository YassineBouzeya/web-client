import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../globals';
import {SearchUsersDto} from '../../model/SearchUsersDto';

@Component({
  selector: 'app-search-users',
  templateUrl: './search-users.component.html',
  styleUrls: ['./search-users.component.css']
})
export class SearchUsersComponent implements OnInit {
  searchUsername: String;
  users: SearchUsersDto[] = [];
  columnsToDisplay = ['username', 'profile'];

  constructor(private http: HttpClient, private globals: Globals, private router: Router) { }

  ngOnInit() {
  }

  searchByUsername() {
    this.http.get<SearchUsersDto[]>(this.globals.backendUrl + '/getUsersContainingUsername/' + this.searchUsername)
      .subscribe(data => this.users = data);
  }

  gotoProfile(user: SearchUsersDto) {
    this.router.navigate(['/profile/' + user.username]);
  }
}
