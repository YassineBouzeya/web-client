import {Component, OnInit} from '@angular/core';
import {UserDto} from '../../model/UserDto';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../globals';
import {Router} from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public newUser: UserDto;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient, private globals: Globals, private router: Router) {
  }

  cancel: boolean;

  ngOnInit() {
    // @ts-ignore
    this.newUser = new UserDto('', '', '', '', '', new Date('01/02/03'));
    this.isAuthenticated();
  }

  onSubmit() {
    this.checkusername(this.newUser.username);
  }


  checkusername(username: String) {
    const _this = this;
    this.http.get<boolean>(this.globals.backendUrl + '/checkusername/' + username, this.httpOptions).subscribe(data => this.cancel = data, null, function () {
      if (!_this.cancel) {
        _this.checkemail(_this.newUser.email);
      } else {
        document.getElementById('usernameError').style.display = 'block';
        return _this.cancel;
      }
    });
    return false;
  }

  checkemail(mail: String) {
    const _this = this;
    this.http.get<boolean>(this.globals.backendUrl + '/checkemail/' + mail, this.httpOptions).subscribe(data => this.cancel = data, null, function () {
      if (!_this.cancel) {
        _this.registerNewUser(_this.newUser);
      } else {
        document.getElementById('emailError').style.display = 'block';
        return _this.cancel;
      }

    });

  }

  registerNewUser(user: UserDto) {
    this.http.post<UserDto>(this.globals.backendUrl + '/register', user, this.httpOptions).subscribe();
    this.router.navigate(['/login']);
  }


  isAuthenticated() {
    const accestoken = localStorage.getItem('access_token');
    if (accestoken != null) {
      this.router.navigate(['/game']);
    }
  }
}
