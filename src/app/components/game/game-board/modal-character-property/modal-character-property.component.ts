import {Component, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../globals';
import {ActivatedRoute, Router} from '@angular/router';
import {CardService} from '../../../../services/card-service/card.service';

import {GameDto} from '../../../../model/GameDto';
import {PlayerDto} from '../../../../model/PlayerDto';
import {ChangeCaracterDto} from '../../../../model/ChangeCaracterDto';
import {GameService} from '../../../../services/game-service/game.service';
import {CharacterpropertyService} from '../../../../services/character-properties-service/characterproperty.service';
import {CharacterDto} from '../../../../model/CharacterDto';

@Component({
  selector: 'app-modal-character-property',
  templateUrl: './modal-character-property.component.html',
  styleUrls: ['./modal-character-property.component.css']
})
export class ModalCharacterPropertyComponent implements OnInit {
  killerId = 1;
  thiefId = 2;
  magicianId = 3;
  kingId = 4;
  bishopId = 5;
  merchantId = 6;
  architectId = 7;
  warlordId = 8;
  charactersMap = new Map<number, boolean>();
  game: GameDto;
  player: PlayerDto;
  userName: String;
  rivals: PlayerDto[];
  characters = [];

  constructor(public activeModal: NgbActiveModal,
              private http: HttpClient,
              private globals: Globals,
              private router: Router,
              private route: ActivatedRoute,
              private cardService: CardService,
              private gameService: GameService,
              private characterpropertyService: CharacterpropertyService) {
  }

  ngOnInit() {
    const _this = this;
    this.game = this.gameService.getGame();
    this.userName = localStorage.getItem('USERNAME');
    this.player = this.game.players.find(value => value.userName === this.userName);
    this.rivals = this.game.players.filter(function (value) {
      return value.userName !== _this.userName;
    });
    this.characters = this.cardService.getCharacters();
    this.determineCharacter(_this);
    this.characters = this.characters.filter(function (value) {
      return value.characterName !== 'Achterkant';
    });
    if (this.charactersMap.get(this.killerId)) {
      this.characters = this.characters.filter(function (value) {
        return value.characterName !== 'Moordenaar';
      });
    }
    if (this.charactersMap.get(this.thiefId)) {
      this.characters = this.characters.filter(function (value) {
        return value.characterName !== 'Dief';
      });
    }
  }

  determineCharacter(_this) {
    this.characters.forEach(function (character: CharacterDto) {
      if (_this.player.characterCard.character.id === character.id) {
        _this.charactersMap.set(character.id, true);
      } else {
        _this.charactersMap.set(character.id, false);
      }
    });
  }


  killCharacter(characterName: String) {
    //this method represents the Killer character property

    const _this = this;
    this.activeModal.close();
    this.characterpropertyService.killCharacter(characterName);

    let charCard = new ChangeCaracterDto();
    charCard.gameId = this.game.id;
    let player = this.game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    charCard.gameDto = this.gameService.getGame();
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
  }

  magicianStealBuildingCards(userName: String) {
    //this method represents the Magician character property
    const _this = this;
    this.gameService.setGame(this.game);
    this.characterpropertyService.magicianStealBuildingCards(userName);
    let charCard = new ChangeCaracterDto();
    charCard.gameId = this.game.id;
    let player = this.game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    charCard.gameDto = this.gameService.getGame();
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
    this.activeModal.close();
  }

  stealFrom(characterName: String) {
    const _this = this;
    this.activeModal.close();
    this.characterpropertyService.stealFrom(characterName);
    let game = this.gameService.getGame();
    let charCard = new ChangeCaracterDto();
    charCard.gameId = game.id;
    let player = game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    game.playerTurn = player.orderNr;
    charCard.gameDto = game;
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
  }

  merchantReceiveCoins() {
    const _this = this;
    this.activeModal.close();
    this.characterpropertyService.merchantReceiveCoins();
    let game = this.gameService.getGame();
    let charCard = new ChangeCaracterDto();
    charCard.gameId = game.id;
    let player = game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    game.playerTurn = player.orderNr;
    charCard.gameDto = game;
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
  }

  kingReceiveCoins() {
    const _this = this;
    this.activeModal.close();
    this.gameService.setGame(this.game);
    this.characterpropertyService.kingReceiveCoins();
    let game = this.gameService.getGame();
    let charCard = new ChangeCaracterDto();
    charCard.gameId = game.id;
    let player = game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    game.playerTurn = player.orderNr;
    charCard.gameDto = game;
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
  }

  bishopReceiveCoins() {
    const _this = this;
    this.activeModal.close();
    this.gameService.setGame(this.game);
    this.characterpropertyService.bishopReceiveCoins();
    let game = this.gameService.getGame();
    let charCard = new ChangeCaracterDto();
    charCard.gameId = game.id;
    let player = game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    game.playerTurn = player.orderNr;
    charCard.gameDto = game;
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
  }

  architectReceiveBuildings() {
    const _this = this;
    this.activeModal.close();
    this.gameService.setGame(this.game);
    this.characterpropertyService.architectReceiveBuildings();
    let game = this.gameService.getGame();
    let charCard = new ChangeCaracterDto();
    charCard.gameId = game.id;
    let player = game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    game.playerTurn = player.orderNr;
    charCard.gameDto = game;
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
  }

  warLordReceiveCoins() {
    const _this = this;
    this.activeModal.close();
    this.gameService.setGame(this.game);
    this.characterpropertyService.warLordReceiveCoins();
    let game = this.gameService.getGame();
    let charCard = new ChangeCaracterDto();
    charCard.gameId = game.id;
    let player = game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    game.playerTurn = player.orderNr;
    charCard.gameDto = game;
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
  }
}
