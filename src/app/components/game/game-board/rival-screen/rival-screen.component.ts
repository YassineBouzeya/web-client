import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PlayerDto} from '../../../../model/PlayerDto';
import {CharacterCardDto} from '../../../../model/CharacterCardDto';
import {CardService} from '../../../../services/card-service/card.service';
import {Globals} from '../../../globals';
import {HttpClient} from '@angular/common/http';
import {BuildingCardDto} from '../../../../model/BuildingCardDto';

@Component({
  selector: 'app-rival-screen',
  templateUrl: './rival-screen.component.html',
  styleUrls: ['./rival-screen.component.css']
})

// This component represents an individual rival
export class RivalScreenComponent implements OnInit {
  @Input() player: PlayerDto;
  @Input() buildingCard: BuildingCardDto;
  @Input() playerTurn;
  @Input() characterTurn;
  @Output() destroyBuilding = new EventEmitter();


  constructor() {
  }

  ngOnInit() {
  }

  destroyMe(eventArgs) {
    this.destroyBuilding.emit(eventArgs);
  }
}
