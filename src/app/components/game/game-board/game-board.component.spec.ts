import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GameBoardComponent} from './game-board.component';
import {AppModule} from '../../app.module';
import {BuildingCardDto} from '../../../model/BuildingCardDto';

describe('GameBoardComponent', () => {
  let component: GameBoardComponent;
  let fixture: ComponentFixture<GameBoardComponent>;
  let spyobj;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      // declarations: [ GameBoardComponent ]
      imports: [AppModule],
      //const dummy = [{}]
    })
      .compileComponents();
    //incrementSpy = spyOn(incrementDecrementService, 'increment').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('removeCoinsAndCheck Should have been called', () => {

    let build = new BuildingCardDto();
   let spy = spyOn(component, 'giveCoinsToThief');
    //component.removeCoinsAndCheck(build);
    expect(component.giveCoinsToThief).toHaveBeenCalled();

  });
});
