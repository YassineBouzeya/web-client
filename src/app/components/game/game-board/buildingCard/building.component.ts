import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BuildingCardDto} from '../../../../model/BuildingCardDto';
import {CardService} from '../../../../services/card-service/card.service';
import {BuildingDto} from '../../../../model/BuildingDto';

@Component({
  selector: 'app-building',
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.css']
})
export class BuildingComponent implements OnInit {
  @Input() buildingCard: BuildingCardDto;
  @Input() isRival: boolean;
  @Output() myBuilding = new EventEmitter();
  path: String;
  building: BuildingDto;
  achterkantPath: String;
  flip = true;

  constructor(private cardService: CardService) {
  }

  ngOnInit() {
    // this.path = this.buildingCard.building.imgPath;
    if (this.isRival) {
      this.flip = false;
    }

    this.building = new BuildingDto();
    this.achterkantPath = this.cardService.getBuilding(99).imgPath; // 'http://i64.tinypic.com/2zez3gl.jpg';
    this.building = this.cardService.getBuilding(this.buildingCard.building.id);
    this.path = this.cardService.getBuilding(this.buildingCard.building.id).imgPath;
    if (this.buildingCard.bought) {
      this.flip = true;
    }
  }

  onClick() {
    if (!this.isRival) {
      this.flip = !this.flip;
      if (!this.flip) {
        this.myBuilding.emit(this.buildingCard);
      }
    } else {
      if (this.buildingCard.bought) {
        this.myBuilding.emit(this.buildingCard);
      }
    }
  }
}
