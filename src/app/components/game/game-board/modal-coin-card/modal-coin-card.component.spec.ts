import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCoinCardComponent } from './modal-coin-card.component';
import {AppModule} from '../../../app.module';
import {GameService} from '../../../../services/game-service/game.service';
import {AuthenticationService} from '../../../../services/security/authentication.service';

describe('ModalCoinCardComponent', () => {
  let component: ModalCoinCardComponent;
  let fixture: ComponentFixture<ModalCoinCardComponent>;
  let service: GameService;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
      //declarations: [ ModalCoinCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCoinCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('check coin method', () => {
    service = TestBed.get(AuthenticationService);

    let gameBefore = service.getGame();
    expect(component.coinsClick).toHaveBeenCalled();
    let gameAfter = service.getGame();
    expect(gameBefore).not.toBe(gameAfter);

  });
});
