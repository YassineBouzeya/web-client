import {Component, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../globals';
import {ActivatedRoute, Router} from '@angular/router';
import {GameService} from '../../../../services/game-service/game.service';
import {BuildingCardDto} from '../../../../model/BuildingCardDto';
import {CardService} from '../../../../services/card-service/card.service';
import {ChangeCaracterDto} from '../../../../model/ChangeCaracterDto';
import {ModalCharacterPropertyComponent} from '../modal-character-property/modal-character-property.component';

@Component({
  selector: 'app-modal-coin-card',
  templateUrl: './modal-coin-card.component.html',
  styleUrls: ['./modal-coin-card.component.css']
})
// Modal for initial choice between coins or a card
export class ModalCoinCardComponent implements OnInit {
  private userName: String;
  private coinPath: String;
  private cardsPath: String;
  private choices: BuildingCardDto[];
  cardBoolean: boolean;
  coinBoolean: boolean;
  private modalRef;
  roundChoosing: boolean;

  constructor(public activeModal: NgbActiveModal,
              private http: HttpClient,
              private globals: Globals,
              private router: Router,
              private route: ActivatedRoute,
              private cardService: CardService,
              private gameService: GameService,
              private modalService: NgbModal) {
  }

  ngOnInit() {
    this.userName = localStorage.getItem('USERNAME');
    this.roundChoosing = this.gameService.getRoundChosing();
    this.cardBoolean = false;
    this.coinBoolean = false;
    this.choices = [];
    this.coinPath = '../../../../assets/coin.png';
    this.cardsPath = '../../../../assets/cards.png';
    this.choices = this.cardService.topTwoBuildings;
    this.globals.stompClient.send('/app/android/buildingCards/' + this.gameService.getGame().id, {}, JSON.stringify({'buildingCards': this.choices}));
  }

  cardClick() {
    this.cardBoolean = true;
    this.coinBoolean = true;
  }

  coinsClick() {
    const _this = this;
    this.activeModal.close();
    this.gameService.addCoinsToPlayer(this.userName);
    this.choices.forEach(function (value) {
      _this.cardService.returnBuildingToStack(value);
    });

    this.gameService.setRoundChosing(true);
    let game = this.gameService.getGame();
    let charCard = new ChangeCaracterDto();
    charCard.gameId = game.id;
    let player = game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    game.playerTurn = player.orderNr;
    charCard.gameDto = game;
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
    this.modalRef = this.modalService.open(ModalCharacterPropertyComponent, {centered: true});
    this.modalRef.componentInstance.name = 'Choose power ';

  }


  buildingSelected(eventArgs) {
    this.activeModal.close();
    const _this = this;
    this.cardService.addBuildingCardToPlayer(eventArgs, this.userName);
    this.choices = this.choices.filter(function (value) {
      return value.id !== eventArgs.id;
    });
    this.choices.forEach(function (value) {
      _this.cardService.discardTheBuilding(value);
    });

    let game = this.gameService.getGame();
    let charCard = new ChangeCaracterDto();
    charCard.gameId = game.id;
    let player = game.players.find(value => value.userName === _this.userName);
    charCard.playerId = player.orderNr;
    game.playerTurn = player.orderNr;
    charCard.gameDto = game;
    this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
    this.modalRef = this.modalService.open(ModalCharacterPropertyComponent, {centered: true});
    this.modalRef.componentInstance.name = 'Choose power ';

  }
}
