import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '../../globals';
import {GameService} from '../../../services/game-service/game.service';
import {GameDto} from '../../../model/GameDto';
import {HttpClient} from '@angular/common/http';
import {PlayerDto} from '../../../model/PlayerDto';
import {CardService} from '../../../services/card-service/card.service';
import {ChangeCaracterDto} from '../../../model/ChangeCaracterDto';
import {JsonConvert} from 'json2typescript';
import {CharacterCardDto} from '../../../model/CharacterCardDto';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalCoinCardComponent} from './modal-coin-card/modal-coin-card.component';
import {TimerComponent} from '../timer/timer.component';
import {BuildingCardDto} from '../../../model/BuildingCardDto';
import {AndroidNotificationDto} from '../../../model/AndroidNotificationDto';
import {PlayerService} from '../../../services/player-service/player.service';
import {SettingsDto} from '../../../model/SettingsDto';
import {CharacterpropertyService} from '../../../services/character-properties-service/characterproperty.service';


@Component({
  providers: [ModalCoinCardComponent, NgbActiveModal, TimerComponent],
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.css'],
})

// This class orchestrates the game logic and keeps the board up to date for everyone
export class GameBoardComponent implements OnInit, OnDestroy {
  private gameId;
  private game: GameDto;
  userSettings: SettingsDto;
  isJoined: boolean;
  isMyTurn: boolean;
  chooseCardTurn: boolean;
  private userName;
  private player: PlayerDto;
  handBuildingCards: BuildingCardDto[] = [];
  boughtBuildingCards: BuildingCardDto[] = [];
  middleFaceUpCharacterCards: CharacterCardDto [] = [];
  middleFaceDownCharacterCards: CharacterCardDto[] = []; //Length is always 1 but doesn't work if not an array
  private rivals: PlayerDto[];
  jsonConvert: JsonConvert = new JsonConvert();
  displayBtn: boolean;
  private modalRef;
  coinCardModalDisplayed: boolean;
  CHARACTERCARD_AMOUNT = 8;
  doneChosing: boolean;
  characterCardsToChooseFrom: CharacterCardDto[] = [];


  constructor(private route: ActivatedRoute,
              private router: Router,
              private globals: Globals,
              private gameService: GameService,
              private http: HttpClient,
              private cardService: CardService,
              private modalService: NgbModal,
              private timerComponentb: TimerComponent,
              private playerService: PlayerService,
              private characterPropertyService: CharacterpropertyService) {

  }

  ngOnDestroy() {
    this.gameService.gameOnDestroy(this.userSettings, this.game.id);
  }

  ngOnInit() {
    this.coinCardModalDisplayed = false;
    this.game = new GameDto();
    this.userSettings = new SettingsDto();
    this.player = new PlayerDto();
    this.rivals = [];
    const _this = this;
    _this.route.params.subscribe(params =>
      _this.gameId = params['id']
    );
    this.userName = localStorage.getItem('USERNAME');
    this.playerService.setuserName(this.userName);
    this.http.get<SettingsDto>(this.globals.backendUrl + '/getSettings').subscribe(function (data) {
      _this.userSettings = data;
      _this.getGameById(_this);
    });
  }

  getGameById(_this) {
    this.http.get<GameDto>(this.globals.backendUrl + '/getGame/' + this.gameId).subscribe(data => this.game = data, null, function () {
      _this.gameService.setGame(_this.game);
      console.log(_this.game);
      _this.doneChosing = _this.game.playersChosen;
      _this.isJoined = _this.game.players.some((player: PlayerDto) => player.userName === _this.userName);
      if (_this.isJoined) {
        _this.player = _this.game.players.find(value => value.userName === _this.userName);
        _this.playerService.setplayer(_this.player);
        _this.rivals = _this.game.players.filter(function (value) {
          return value.userName !== _this.userName;
        });
        _this.setPlayerCards();
        _this.placeMiddleCards();
        _this.isMyTurn = _this.game.playerTurn === _this.player.orderNr;
        _this.chooseCardTurn = _this.allPlayersChosen();
        _this.globals.initializeWebSocketConnection(function () {
          if (_this.isMyTurn) {
            if(!_this.doneChosing){
              _this.characterCardsToChooseFrom = _this.game.characterCards.filter(c => !c.placedMiddle);
            }
            _this.sendAndroidMyTurnNotif();
          }
          if (_this.gameService.androidConnected) {
            _this.subAndroidAsignCharacter();
            _this.subAndroidCoinsClick();
            _this.subAndroidCardsClick();
            _this.subAndroidBuildingSelected();
            _this.sendAndroidRemoteMyTurn();
          }
          _this.globals.stompClient.unsubscribe('myTurnNotif');
          _this.globals.stompClient.unsubscribe('otherPlayedNotif');
          _this.subReceiveChange();
          _this.subRefreshGame();
          _this.subConnectAndroid();
          _this.subFastRefresh();
          _this.subResetRound();
        });
        //
        // let charCard = new ChangeCaracterDto();
        // charCard.gameId = _this.game.id;
        // let player = _this.game.players.find(value => value.userName === _this.userName);
        // charCard.playerId = player.orderNr;
        // _this.game.playerTurn = player.orderNr;
        // charCard.gameDto = _this.game;
        // _this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
      }
    });
  }

  sendAndroidMyTurnNotif() {
    if (this.userSettings.myTurnNotifications) {
      let androidNotDto = new AndroidNotificationDto();
      androidNotDto.username = this.userName;
      androidNotDto.gameId = this.gameId;
      androidNotDto.gameTitle = this.game.gameName;
      androidNotDto.content = 'Jij bent aan de beurt in ' + this.game.gameName;
      this.gameService.sendAndroidNotification(androidNotDto);
    }
  }

  sendAndroidRemoteMyTurn() {
    this.globals.stompClient.send('/app/android/myTurn/' + this.gameId, {}, JSON.stringify({
      'isChooseCardTurn': String(this.chooseCardTurn),
      'gameDto': this.game
    }));
  }

  onGameChanged(eventargs) {
    this.game.characterCards = eventargs;
  }

  nextCharacter() {
    let charCard = new ChangeCaracterDto();
    charCard.gameId = this.game.id;
    charCard.playerId = this.player.orderNr;
    charCard.gameDto = this.game;
    this.globals.stompClient.send('/app/nextCharacter', {}, JSON.stringify(charCard));
  }

  asignCharacter(eventArgs) {
    this.player.characterCard = this.game.characterCards.find(value => value.id === eventArgs.id);
    this.game.characterCards = this.game.characterCards.filter(function (value) {
      return value.id !== eventArgs.id;
    });
    let charCard = new ChangeCaracterDto();
    charCard.gameId = this.gameId;
    charCard.characterCardId = eventArgs.id;
    charCard.playerId = this.player.orderNr;
    charCard.gameDto = this.game;
    this.globals.stompClient.send('/app/characterSelected', {}, JSON.stringify(charCard));
  }

  subReceiveChange() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/receiveChange/' + _this.game.id, function (game) {
      _this.game = JSON.parse(game.body);
      _this.characterCardsToChooseFrom = _this.game.characterCards.filter(c => !c.placedMiddle);
      _this.gameService.setGame(_this.game);
      _this.doneChosing = _this.game.playersChosen;
      if (_this.game.playersChosen) {
        let game = _this.gameService.getGame();
        let charCard = new ChangeCaracterDto();
        charCard.gameId = game.id;
        let player = _this.player;
        charCard.playerId = player.orderNr;
        charCard.gameDto = game;
        _this.globals.stompClient.send('/app/nextCharacter', {}, JSON.stringify(charCard));
      } else {
        _this.player = _this.game.players.find(value => value.userName === _this.userName);
        _this.playerService.setplayer(_this.player);
        _this.isMyTurn = Number(_this.game.playerTurn) === Number(_this.player.orderNr);
        _this.rivals = _this.game.players.filter(function (value) {
          return value.userName !== _this.userName;
        });
        _this.setPlayerCards();
        _this.gameService.setRoundChosing(_this.allPlayersChosen());
        _this.chooseCardTurn = _this.allPlayersChosen();
        _this.displayBtn = _this.displayAheadOption();
        if (_this.isMyTurn) {
          _this.sendAndroidMyTurnNotif();
          if (_this.gameService.androidConnected) {
            _this.globals.stompClient.send('/app/android/myTurn/' + _this.gameId, {}, JSON.stringify({
              'isChooseCardTurn': String(_this.chooseCardTurn),
              'gameDto': _this.game
            }));
          }
        }
      }
    }, {id: 'change' + _this.game.id});
  }

  subFastRefresh() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/fastRefresh/' + _this.game.id, function (game) {
      _this.game = JSON.parse(game.body);
      _this.gameService.setGame(_this.game);
      _this.doneChosing = _this.game.playersChosen;
      _this.player = _this.game.players.find(value => value.userName === _this.userName);
      //PLAYER SKIP TURN
      if (_this.player.characterCard.killed) {
        _this.isMyTurn = false;
      } else {
        _this.isMyTurn = Number(_this.game.playerTurn) === Number(_this.player.orderNr);
      }
      _this.rivals = _this.game.players.filter(function (value) {
        return value.userName !== _this.userName;
      });
      _this.setPlayerCards();
      _this.displayBtn = _this.displayAheadOption();
      if (_this.player.characterCard.robbed && _this.isMyTurn) {
        _this.giveCoinsToThief();

        let charCard = new ChangeCaracterDto();
        charCard.gameId = game.id;
        charCard.playerId = _this.player.orderNr;
        charCard.gameDto = _this.gameService.getGame();
        _this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
      }
      if (_this.isMyTurn && _this.gameService.androidConnected) {
        _this.sendAndroidRemoteMyTurn();
      }
    }, {id: 'fastrefresh' + _this.game.id});
  }

  subRefreshGame() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/refreshGame/' + _this.game.id, function (game) {
      _this.game = JSON.parse(game.body);// _this.gameService.getGame();//JSON.parse(game.body);
      _this.gameService.setGame(_this.game);
      _this.doneChosing = _this.game.playersChosen;
      _this.player = _this.game.players.find(value => value.userName === _this.userName);
      _this.playerService.setplayer(_this.player);
      //PLAYER SKIP TURN
      if (_this.player.characterCard.killed) {
        _this.isMyTurn = false;
        let charCard = new ChangeCaracterDto();
        charCard.gameId = game.id;
        charCard.playerId = _this.player.orderNr;
        charCard.gameDto = game;
        _this.globals.stompClient.send('/app/simpleRefresh', {}, JSON.stringify(charCard));
      } else {
        _this.isMyTurn = Number(_this.game.playerTurn) === Number(_this.player.orderNr);
      }
      if (_this.player.characterCard.robbed && _this.isMyTurn) {
        _this.giveCoinsToThief();
        let charCard = new ChangeCaracterDto();
        charCard.gameId = game.id;
        charCard.playerId = _this.player.orderNr;
        charCard.gameDto = _this.gameService.getGame();
        _this.globals.stompClient.send('/app/addCoins', {}, JSON.stringify(charCard));
      }
      _this.rivals = _this.game.players.filter(function (value) {
        return value.userName !== _this.userName;
      });
      _this.setPlayerCards();
      _this.displayBtn = _this.displayAheadOption();
      _this.gameService.setRoundChosing(false);
      if (_this.isMyTurn) {
        _this.sendAndroidMyTurnNotif();
        if (_this.gameService.androidConnected) {
          _this.sendAndroidRemoteMyTurn();
        }
      }
    }, {id: 'refresh' + _this.game.id});
  }


  subConnectAndroid() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/android/connect/' + this.userName + '/' + this.gameId, function (canConnect) {
      if (canConnect.body === 'true') {
        _this.gameService.androidConnected = true;
        _this.subAndroidAsignCharacter();
        _this.subAndroidCoinsClick();
        _this.subAndroidCardsClick();
        _this.subAndroidBuildingSelected();
      }
    }, {id: 'androidConnect' + _this.gameId});
  }

  subAndroidAsignCharacter() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/android/characterSelected/' + this.userName + '/' + this.game.id, function (character) {
      const jsonObj: object = JSON.parse(character.body);
      const characterObj = _this.jsonConvert.deserializeObject(jsonObj, CharacterCardDto);
      _this.asignCharacter(characterObj);
    }, {id: 'androidChooseCharacter' + _this.gameId});
  }

  subAndroidCoinsClick() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/android/coinsClick/' + this.userName + '/' + this.game.id, function () {
      _this.modalRef.componentInstance.coinsClick();
    }, {id: 'androidCoinsClick' + _this.gameId});
  }

  subAndroidCardsClick() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/android/cardsClick/' + this.userName + '/' + this.game.id, function () {
      _this.modalRef.componentInstance.cardClick();
    }, {id: 'androidCardsClick' + _this.gameId});
  }

  subAndroidBuildingSelected() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/android/buildingSelected/' + this.userName + '/' + this.game.id, function (buildingCard) {
      const jsonObj: object = JSON.parse(buildingCard.body);
      const buildingObj = _this.jsonConvert.deserializeObject(jsonObj, BuildingCardDto);
      _this.modalRef.componentInstance.buildingSelected(buildingObj);
    }, {id: 'androidBuildingSelected' + _this.gameId});
  }

  subResetRound() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/resetRound/' + _this.game.id, function (game) {
      _this.game = JSON.parse(game.body);
      _this.gameService.setGame(_this.game);
      if (_this.game.gameOver) {
        _this.router.navigate(['/score/', _this.gameId]);
      } else {
        _this.doneChosing = _this.game.playersChosen;
        _this.placeMiddleCards();
        _this.player = _this.game.players.find(value => value.userName === _this.userName);
        _this.isMyTurn = _this.game.playerTurn === _this.player.orderNr;
        _this.chooseCardTurn = _this.allPlayersChosen();
        if (_this.isMyTurn) {
          _this.sendAndroidMyTurnNotif();
          if (_this.gameService.androidConnected) {
            _this.sendAndroidRemoteMyTurn();
          }
        }
      }
    }, {id: 'resetRound' + _this.game.id});
  }

  allPlayersChosen() {
    let charLength = this.game.characterCards.length;
    let playerLength = this.game.players.length;
    let enough = (this.CHARACTERCARD_AMOUNT - playerLength) === charLength;
    return !enough;
  }

  open() {
    if (!this.player.characterCard.killed) {
      this.timerComponentb.resetTimer();
      this.gameService.setGame(this.game);
      this.modalRef = this.modalService.open(ModalCoinCardComponent, {centered: true});
      this.modalRef.componentInstance.name = 'CoinCards';
    }
  }

  displayAheadOption() {
    let playersDone = !this.allPlayersChosen();
    if (playersDone && this.isMyTurn) {
      if (!this.coinCardModalDisplayed) {
        this.open();
        this.coinCardModalDisplayed = true;
      }
      return true;
    } else {
      return false;
    }
  }

  giveCoinsToThief() {
    const _this = this;
    let thief = this.game.players.find(value => value.characterCard.character.id === 2);
    thief.nrOfCoins = thief.nrOfCoins + this.player.nrOfCoins;
    this.player.nrOfCoins = 0;
    this.player.characterCard.robbed = false;
    this.game.players.forEach(value => {
      if (value.characterCard.character.id === 2) {
        value = thief;
      }
      if (value.userName === _this.userName) {
        value = _this.player;
      }
    });
    this.gameService.setGame(this.game);
  }


  setPlayerCards() {
    this.boughtBuildingCards = this.player.buildingCards.filter(b => b.bought);
    this.handBuildingCards = this.player.buildingCards.filter(b => !b.bought);
    for (let rival of this.rivals) {
      rival.handBuildingCards = rival.buildingCards.filter(b => !b.bought);
      rival.boughtBuildingCards = rival.buildingCards.filter(b => b.bought);
    }
  }

  placeMiddleCards() {
    this.middleFaceDownCharacterCards = this.game.characterCards.filter(c => c.placedMiddle && !c.faceUp);
    this.middleFaceUpCharacterCards = this.game.characterCards.filter(c => c.faceUp && c.placedMiddle);
    this.characterCardsToChooseFrom = this.game.characterCards.filter(c => !c.placedMiddle);
  }

  warLordDestroyBuilding(eventArgs) {
    const _this = this;
    if (this.player.characterCard.character.characterName === 'Condottiere') {
      this.characterPropertyService.warLordRemoveBuilding(eventArgs);
      let game = this.gameService.getGame();
      let charCard = new ChangeCaracterDto();
      charCard.gameId = game.id;
      let player = game.players.find(value => value.userName === _this.userName);
      charCard.playerId = player.orderNr;
      game.playerTurn = player.orderNr;
      charCard.gameDto = game;
      this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
    }
  }
}
