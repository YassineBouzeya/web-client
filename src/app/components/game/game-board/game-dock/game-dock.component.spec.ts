import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameDockComponent } from './game-dock.component';

describe('GameDockComponent', () => {
  let component: GameDockComponent;
  let fixture: ComponentFixture<GameDockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameDockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameDockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
