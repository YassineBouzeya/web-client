import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PlayerDto} from '../../../../model/PlayerDto';
import {CardService} from '../../../../services/card-service/card.service';
import {BuildingCardDto} from '../../../../model/BuildingCardDto';
import {CharacterCardDto} from '../../../../model/CharacterCardDto';
import {Globals} from '../../../globals';
import {HttpClient} from '@angular/common/http';
import {TimerComponent} from '../../timer/timer.component';
import {ChangeCaracterDto} from '../../../../model/ChangeCaracterDto';
import {GameService} from '../../../../services/game-service/game.service';

@Component({
  selector: 'app-game-dock',
  templateUrl: './game-dock.component.html',
  styleUrls: ['./game-dock.component.css']
})

// Represents my inventory
export class GameDockComponent implements OnInit {
  @Input() player: PlayerDto;
  @Input() characters: CharacterCardDto[];
  @Input() gameId: number;
  @Output() public gameChanged = new EventEmitter();
  @Input() buildingCards: BuildingCardDto[] = [];
  @Input() handBuildingCards: BuildingCardDto[] = [];
  @Input() boughtBuildingCards: BuildingCardDto[] = [];
  @Input() verderBtn: boolean;
  @Input() myTurn: boolean;
  @Input() doneChosing: boolean;

  constructor(private cardService: CardService,
              private globals: Globals,
              private http: HttpClient,
              private timerComponent: TimerComponent,
              private gameService: GameService) {
  }

  ngOnInit() {
  }


  buyBuilding(building) {
    if (this.myTurn) {
      this.cardService.buyBuilding(building, this.player.userName);
      let game = this.gameService.getGame();
      let charCard = new ChangeCaracterDto();
      charCard.gameId = game.id;
      charCard.playerId = this.player.orderNr;
      charCard.gameDto = game;
      this.globals.stompClient.send('/app/fastRefresh', {}, JSON.stringify(charCard));
    }
  }
}
