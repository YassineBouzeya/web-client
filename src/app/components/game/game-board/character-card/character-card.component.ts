import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CharacterCardDto} from '../../../../model/CharacterCardDto';
import {CardService} from '../../../../services/card-service/card.service';
import {CharacterDto} from '../../../../model/CharacterDto';

@Component({
  selector: 'app-character-card',
  templateUrl: './character-card.component.html',
  styleUrls: ['./character-card.component.css']
})
export class CharacterCardComponent implements OnInit {
  @Input() characterCard: CharacterCardDto;
  @Output() public change = new EventEmitter();
  @Input() isRival: boolean;
  path: String;
  achterkantPath: String;
  character: CharacterDto;
  flip = true;

  constructor(private cardService: CardService) {
  }

  ngOnInit() {
    this.achterkantPath = this.cardService.getCharacter(99).imgPath; // 'http://i65.tinypic.com/bi1cfk.jpg';
    this.character = this.cardService.getCharacter(this.characterCard.character.id);
    this.path = this.character.imgPath;
    if (this.isRival){
      this.flip = false;
    }
  }

  onClick() {
    if (!this.isRival){
      if (this.flip) {
        this.change.emit(this.characterCard);
      }
    }
  }
}
