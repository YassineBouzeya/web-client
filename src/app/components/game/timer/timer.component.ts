import { Component, OnInit } from '@angular/core';
import {interval} from 'rxjs';
import {Globals} from '../../globals';
import {GameService} from '../../../services/game-service/game.service';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {

  constructor(private globals: Globals, private gameService: GameService ) { }


  timeLeft: number = 0.0;
  interval;

  ngOnInit() {
    this.runTimer();
  }
  runTimer(){
    const _this = this;
    this.timeLeft = this.gameService.getGame().turnDuration;

    setInterval(() => {
      if(this.timeLeft > 0) {

        this.timeLeft-=0.01;
        this.timeLeft=Math.round(this.timeLeft*100)/100;

      } else {
        this.timeLeft = 60;
      }

  },10)
  }

  resetTimer() {
    this.timeLeft=60;
    clearInterval(this.timeLeft);
    //this.timeLeft = this.gameService.getGame().turnDuration;
    //this.interval;
    this.runTimer();
  }
}
