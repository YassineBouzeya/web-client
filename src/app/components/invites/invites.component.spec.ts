import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitesComponent } from './invites.component';
import {AppModule} from '../app.module';

describe('InvitesComponent', () => {
  let component: InvitesComponent;
  let fixture: ComponentFixture<InvitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ InvitesComponent ]
      imports:[AppModule]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
