import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../globals';
import {InviteDto} from '../../model/InviteDto';
import {GameDto} from '../../model/GameDto';
import {Router} from '@angular/router';
import * as $ from 'jquery';
import {JsonConvert} from 'json2typescript';

@Component({
  selector: 'app-invites',
  templateUrl: './invites.component.html',
  styleUrls: ['./invites.component.css']
})
export class InvitesComponent implements OnInit {
  invites: InviteDto[] = [];
  columnsToDisplay = ['remove', 'invitingUserName', 'gameName', 'turnDuration', 'reqBuildings', 'maxPlayers', 'joinType', 'play'];
  freshGame: GameDto;
  isFull = false;
  jsonConvert: JsonConvert = new JsonConvert();

  constructor(private http: HttpClient, private globals: Globals, private router: Router) {
  }

  ngOnInit() {
    this.subIncomingInvites();
    this.http.get<InviteDto[]>(this.globals.backendUrl + '/getInvites').subscribe(data => this.invites = data, null, function () {
      $(document).ready(function () {
        $('.joinType').text(function () {
          if (this.innerHTML === '0') {
            return 'Open';
          }
          if (this.innerHTML === '1') {
            return 'Privé';
          }
        });
      });
    });
  }

  joinGame(invite: InviteDto) {
    let game = invite.gameDto;
    const _this = this;
    this.freshGame = new GameDto();
    this.http.get<GameDto>(this.globals.backendUrl + '/getLiteGame/' + game.id).subscribe(data => this.freshGame = data, null, function () {
      if (_this.freshGame.nrOfPlayers !== _this.freshGame.maxPlayers) {
        _this.globals.stompClient.subscribe('/r/lobby/' + game.id, function () {
          _this.router.navigate(['/lobby', game.id]);
          _this.remove(invite);
        }, {id: 'lobby' + game.id});
        _this.globals.stompClient.send('/app/joinLobby', {}, JSON.stringify({'id': game.id}));
      }
    });
  }

  remove(invite: InviteDto) {
    const _this = this;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post(this.globals.backendUrl + '/removeInvite', invite, httpOptions).subscribe(null, null, function () {
      let i = _this.invites.findIndex(i => i === invite);
      _this.invites.splice(i, 1);
      _this.invites = _this.invites.slice();
    });
  }

  subIncomingInvites() {
    const _this = this;
    this.globals.initializeWebSocketConnection(function () {
      _this.globals.stompClient.subscribe('/user/r/invite', function (invite) {
        const jsonObj: object = JSON.parse(invite.body);
        const newInvite = _this.jsonConvert.deserializeObject(jsonObj, InviteDto);
        _this.invites.unshift({
          id: newInvite.id,
          invitedUserName: newInvite.invitingUserName,
          invitingUserName: newInvite.invitingUserName,
          gameDto: newInvite.gameDto,
        });
        _this.invites = _this.invites.slice();
        $(document).ready(function () {
          $('.joinType').text(function () {
            if (this.innerHTML === '0') {
              return 'Open';
            }
            if (this.innerHTML === '1') {
              return 'Privé';
            }
          });
        });
      }, {id: 'invitesList'});
    });
  }
}
