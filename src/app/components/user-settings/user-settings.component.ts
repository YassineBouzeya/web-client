import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../globals';
import {SettingsDto} from '../../model/SettingsDto';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {
  settingsDto: SettingsDto;
  showSuccess = false;

  constructor(private http: HttpClient, private globals: Globals) {
  }

  ngOnInit() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const _this = this;
    this.http.get<SettingsDto>(this.globals.backendUrl + '/getSettings', httpOptions).subscribe(function (data) {
      _this.settingsDto = data;
    });
  }


  setFriendRequestNotifs() {
    this.settingsDto.friendRequestNotifications = !this.settingsDto.friendRequestNotifications;
  }

  setInviteNotifs() {
    this.settingsDto.inviteNotifications = !this.settingsDto.inviteNotifications;
  }

  setPlayerPlayedNotifs() {
    this.settingsDto.otherTurnNotifications = !this.settingsDto.otherTurnNotifications;
  }

  setMyTurnNotifs() {
    this.settingsDto.myTurnNotifications = !this.settingsDto.myTurnNotifications;
  }

  setTurnExpiresNotifs() {
    this.settingsDto.turnIsExpiringNotifications = !this.settingsDto.turnIsExpiringNotifications;
  }

  setChatNotifs() {
    this.settingsDto.chatNotifications = !this.settingsDto.chatNotifications;
  }

  setPrivacy() {

    this.settingsDto.public = !this.settingsDto.public;
    this.saveSettings();
  }

  saveSettings() {
    const _this = this;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post<SettingsDto>(this.globals.backendUrl + '/saveSettings', this.settingsDto, httpOptions).subscribe();
    _this.showSuccess = true;
  }

}
