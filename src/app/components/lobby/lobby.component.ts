import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '../globals';
import {GameService} from '../../services/game-service/game.service';
import {GameDto} from '../../model/GameDto';
import {PlayerDto} from '../../model/PlayerDto';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalFriendsComponent} from './modal-friends/modal-friends.component';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css'],
})
export class LobbyComponent implements OnInit {
  players: PlayerDto[] = [];
  gameId: number;
  game: GameDto;
  gamePin: string;
  userName;
  hostName;
  isJoined: boolean = false;
  lobbyReady = false;

  constructor(private route: ActivatedRoute, private router: Router, private globals: Globals,
              private gameService: GameService, private modalService: NgbModal, private http: HttpClient) {

  }

  ngOnInit() {
    let pageURL = window.location.href;
    this.gamePin = pageURL.substr(pageURL.lastIndexOf('/') + 1);
    const _this = this;
    this.userName = localStorage.getItem('USERNAME');
    this.route.params.subscribe(params => this.gameId = params['id']);
    this.http.get<GameDto>(this.globals.backendUrl + '/getLiteGame/' + this.gameId).subscribe(data => this.game = data, null, function () {
      _this.gameService.setGame(_this.game);
      if (_this.game.status === 0) {
        _this.http.get<PlayerDto[]>(_this.globals.backendUrl + '/getLobbyPlayers/' + _this.gameId).subscribe(data => _this.players = data, null, function () {
          _this.isJoined = _this.players.some((player: PlayerDto) => player.userName === _this.userName);
          if (_this.isJoined) {
            _this.globals.initializeWebSocketConnection(function () {
              _this.subLobby();
              _this.subStartGame();
              _this.subLeavingPlayers();
              _this.subConnectAndroid();
              _this.lobbyReady = true;
            });
            _this.hostName = _this.players.find((player: PlayerDto) => player.host === true).userName;
          }
        });
      }
    });
   }

  startGame() {
    this.globals.stompClient.send('/app/startGame', {}, JSON.stringify(this.game));
  }

  open() {
    const modalRef = this.modalService.open(ModalFriendsComponent, {centered: true});
    modalRef.componentInstance.name = 'Friends';
  }

  subLobby() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/lobby/' + _this.gameId, function (players) {
      _this.players = JSON.parse(players.body);
      _this.gameService.setPlayers(_this.players);
      _this.players = _this.players.slice();
      if(_this.players.length===_this.game.maxPlayers&&_this.userName===_this.hostName){
        _this.startGame();
      }
    }, {id: 'lobby' + _this.gameId});
  }

  subStartGame() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/startGame/' + _this.gameId, function (game) {
      _this.game = JSON.parse(game.body);
      _this.gameService.setGame(_this.game);
      _this.router.navigate(['/game/', _this.gameId]);
    }, {id: 'start' + _this.gameId});
  }

  subLeavingPlayers() {
    const _this = this;
    _this.globals.stompClient.subscribe('/r/leaveLobby/' + _this.gameId, function (player) {
      _this.players.splice(_this.players.findIndex(p => p.userName === JSON.parse(player.body).userName), 1);
      _this.players = _this.players.slice();
    }, {id: 'leaveLobby' + _this.gameId});
  }
  subConnectAndroid() {
    const _this = this;
    this.globals.stompClient.subscribe('/r/android/connect/' + this.userName + '/' + this.gameId, function (canConnect) {
      if (canConnect.body === 'true') {
        _this.gameService.androidConnected=true;
      }
    });
  }
}
