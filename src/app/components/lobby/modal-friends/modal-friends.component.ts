import {Component, Input, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FriendDto} from '../../../model/FriendDto';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../globals';
import {ActivatedRoute, Router} from '@angular/router';
import {InviteDto} from '../../../model/InviteDto';
import {GameService} from '../../../services/game-service/game.service';
@Component({
  selector: 'app-modal-friends',
  templateUrl: './modal-friends.component.html',
  styleUrls: ['./modal-friends.component.css']
})
export class ModalFriendsComponent implements OnInit {
  friends: FriendDto[] = [];
  prevPlayers: String[]=[];
  gameId: number;
  @Input() friendName;
  @Input() email;
  constructor(public activeModal: NgbActiveModal, private http: HttpClient, private globals: Globals, private router: Router, private route: ActivatedRoute, private gameService: GameService) {}

  ngOnInit() {
    this.http.get<FriendDto[]>(this.globals.backendUrl + '/getFriends').subscribe(data => this.friends = data);
    this.http.get<String[]>(this.globals.backendUrl + '/getPreviousPlayers/'+localStorage.getItem('USERNAME')).subscribe(data => this.prevPlayers = data);
    this.route.params.subscribe(params =>
      this.gameId = params['id']
    );
  }

  invite(friend: FriendDto) {
    let invite = new InviteDto();
    invite.gameDto=this.gameService.getGame();
    invite.invitedUserName=friend.userName;
    this.globals.stompClient.send('/app/invite', {}, JSON.stringify(invite))
  }

  inviteByUserName() {
    let invite = new InviteDto();
    invite.gameDto=this.gameService.getGame();
    invite.invitedUserName=this.friendName;
    this.globals.stompClient.send('/app/invite', {}, JSON.stringify(invite))
  }
  inviteByUserNamePrev(username:String) {
    let invite = new InviteDto();
    invite.gameDto=this.gameService.getGame();
    invite.invitedUserName=username;
    this.globals.stompClient.send('/app/invite', {}, JSON.stringify(invite))
  }

}
