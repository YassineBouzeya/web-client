import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFriendsComponent } from './modal-friends.component';
import {AppModule} from '../../app.module';

describe('ModalFriendsComponent', () => {
  let component: ModalFriendsComponent;
  let fixture: ComponentFixture<ModalFriendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ ModalFriendsComponent ]
      imports:[AppModule]

    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFriendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
