import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherProfileComponent } from './other-profile.component';
import {AppModule} from '../app.module';

describe('OtherProfileComponent', () => {
  let component: OtherProfileComponent;
  let fixture: ComponentFixture<OtherProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      //declarations: [ OtherProfileComponent ]
      imports: [AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
