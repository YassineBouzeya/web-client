import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserDto} from '../../model/UserDto';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../globals';
import {DatePipe} from '@angular/common';
import {StatisticsDto} from '../../model/StatisticsDto';
import {PrivacyDto} from '../../model/PrivacyDto';

@Component({
  selector: 'app-other-profile',
  templateUrl: './other-profile.component.html',
  styleUrls: ['./other-profile.component.css']
})
export class OtherProfileComponent implements OnInit {
  username: string;
  showStatistics: boolean;
  isProfilePublic: boolean;
  user: UserDto;
  userBirthdayString: String;
  statistics: StatisticsDto;
  private pipe = new DatePipe('en-US');

  constructor(private route: ActivatedRoute, private http: HttpClient, private globals: Globals) {
  }

  ngOnInit() {
    const _this = this;
    this.showStatistics = false;
    this.route.params.subscribe(params => this.username = params['username']);
    this.http.get<PrivacyDto>(this.globals.backendUrl + '/getPrivacySettings/' + this.username).subscribe(function (data) {
      _this.isProfilePublic = data.public;
      if (_this.isProfilePublic) {
        _this.http.get<UserDto>(_this.globals.backendUrl + '/getUser/' + _this.username).subscribe(function (getUserData) {
          _this.user = getUserData;
          _this.user.birthday = new Date(_this.pipe.transform(_this.user.birthday, 'longDate'));
          _this.userBirthdayString = getUserData.birthday.getDay() + '/' + getUserData.birthday.getMonth() + '/'
            + getUserData.birthday.getFullYear();
        });
        _this.http.get<StatisticsDto>(_this.globals.backendUrl + '/getStatistics/' + _this.username)
          .subscribe( getStatisticsData => _this.statistics = getStatisticsData);
      }
    });
  }

  switchView(text) {
    if (text === this.username + '\'s statistieken' && !this.showStatistics) {
      this.showStatistics = true;
    } else if (text === this.username + '\'s profiel' && this.showStatistics) {
      this.showStatistics = false;
    }
  }
}
