import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Globals} from '../globals';
import {GameService} from '../../services/game-service/game.service';
import {GameDto} from '../../model/GameDto';
import * as $ from 'jquery';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-game-overview',
  templateUrl: './game-overview.component.html',
  styleUrls: ['./game-overview.component.css']
})
export class GameOverviewComponent implements OnInit {

  games: GameDto[] = [];
  game: GameDto;
  columnsToDisplay = ['gameName', 'turnDuration', 'reqBuildings', 'joinType', 'status', 'maxPlayers', 'play', 'leave','score'];

  constructor(private route: ActivatedRoute, private router: Router, private globals: Globals, private gameService: GameService, private http: HttpClient) {
  }

  ngOnInit() {
    const _this = this;
    this.globals.initializeWebSocketConnection();
    this.http.get<GameDto[]>(this.globals.backendUrl + '/gameOverview').subscribe(data => {
      _this.games = data;
      _this.games = _this.games.slice();
      $(document).ready(function () {
        $('.joinType').text(function () {
          if (this.innerHTML === '0') {
            return 'Open';
          }
          if (this.innerHTML === '1') {
            return 'Privé';
          }
        });
        $('.status').text(function () {
          if (this.innerHTML === '0') {
            $(this).addClass('lobby');
            return 'In Lobby';
          }
          if (this.innerHTML === '1') {
            $(this).addClass('ongoing');
            return 'Lopend';
          }
          if (this.innerHTML === '2') {
            $(this).addClass('ended');
            return 'Geëindigd';
          }
        });
      });
    });
  }

  joinGame(game: GameDto) {
    const _this = this;
    this.http.get<GameDto>(this.globals.backendUrl + '/getGame/' + game.id).subscribe(data => this.game = data, null, function () {
      _this.gameService.setGame(_this.game);
      if (_this.game.status === 0) {
        _this.globals.stompClient.send('/app/joinLobby', {}, JSON.stringify({'id': game.id}));
        _this.router.navigate(['/lobby', game.id]);
      }
      if (_this.game.status === 1) {
        _this.globals.stompClient.subscribe('/r/lobby/' + game.id, function () {
          _this.router.navigate(['/game', game.id]);
        }, {id: 'lobby' + game.id});
        _this.globals.stompClient.send('/app/joinLobby', {}, JSON.stringify({'id': game.id}));
      }
    });
  }

  leaveGame(game: GameDto) {
    this.globals.stompClient.send('/app/leaveGame', {}, JSON.stringify({'id': game.id}));
    this.games.splice(this.games.findIndex(g => g.id === game.id), 1);
    this.games = this.games.slice();
  }

  goToScore(game: GameDto) {
    this.router.navigate(['/score', game.id]);
  }
}
