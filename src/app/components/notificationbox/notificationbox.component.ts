import {Component, OnInit} from '@angular/core';
import {Globals} from '../globals';
import {FriendRequestDto} from '../../model/FriendRequestDto';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FriendDto} from '../../model/FriendDto';
import * as $ from 'jquery';
import {InviteDto} from '../../model/InviteDto';
import {GameDto} from '../../model/GameDto';
import {Router} from '@angular/router';
import {PlayerDto} from '../../model/PlayerDto';
import {ChatMessageDto} from '../../model/ChatMessageDto';
import {SettingsDto} from '../../model/SettingsDto';

@Component({
  selector: 'app-notificationbox',
  templateUrl: './notificationbox.component.html',
  styleUrls: ['./notificationbox.component.css']
})
export class NotificationboxComponent implements OnInit {
  friendRequest: FriendRequestDto = null;
  invite: InviteDto = null;
  freshGame: GameDto = null;
  static myTurnGame: GameDto = null;
  static otherTurnGame: GameDto = null;
  static chatMessage: ChatMessageDto = null;
  notificationBoxComponent = NotificationboxComponent;
  playedPlayer: PlayerDto = null;
  userSettings: SettingsDto;

  constructor(private globals: Globals, private http: HttpClient, private router: Router) {
  }

  ngOnInit() {
    const _this = this;
    this.http.get<SettingsDto>(this.globals.backendUrl + '/getSettings').subscribe(data => this.userSettings = data, error1 => {console.log(error1)}, function () {
      _this.globals.initializeWebSocketConnection(function () {
        if (_this.userSettings.friendRequestNotifications) {
          _this.subIncomingFriendRequests();
        }
        if (_this.userSettings.inviteNotifications) {
          _this.subInvites();
        }
        if (_this.userSettings.myTurnNotifications) {
          _this.subMyTurn();
        }
        if (_this.userSettings.otherTurnNotifications) {
          _this.subOtherPlayed();
        }
        if (_this.userSettings.chatNotifications) {
          _this.subChat();
        }
      });
    });

    $('#friendRequest').hide();
    $('#gameInvite').hide();
    $('#myTurn').hide();
    $('#otherPlayed').hide();
    $('#chatMessage').hide();
  }

  subIncomingFriendRequests() {
    const _this = this;
    _this.globals.stompClient.subscribe('/user/r/friendRequest', function (friendRequest) {
      _this.friendRequest = JSON.parse(friendRequest.body);
      $('#gameInvite').hide();
      $('#myTurn').hide();
      $('#otherPlayed').hide();
      $('#chatMessage').hide();
      $('#friendRequest').show();
    }, {id: 'receivedFriendRequestsNotif'});
  }

  acceptRequest() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post<FriendDto>(this.globals.backendUrl + '/acceptRequest', this.friendRequest.sender, httpOptions).subscribe();
    $('#friendRequest').hide();
  }

  denyRequest() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post(this.globals.backendUrl + '/denyRequest', this.friendRequest.sender, httpOptions).subscribe();
    $('#friendRequest').hide();
  }

  subInvites() {
    const _this = this;
    this.globals.stompClient.subscribe('/user/r/invite', function (invite) {
      _this.invite = JSON.parse(invite.body);
      $('#myTurn').hide();
      $('#friendRequest').hide();
      $('#chatMessage').hide();
      $('#otherPlayed').hide();
      $('#gameInvite').show();
    }, {id: 'invitesNotif'});

  }


  subMyTurn() {
    const _this = this;
    this.globals.stompClient.subscribe('/user/r/myTurn', function (game) {
      NotificationboxComponent.myTurnGame = JSON.parse(game.body);
      $('#friendRequest').hide();
      $('#gameInvite').hide();
      $('#chatMessage').hide();
      $('#otherPlayed').hide();
      $('#myTurn').show();
    }, {id: 'myTurnNotif'});
  }

  subOtherPlayed() {
    const _this = this;
    return this.globals.stompClient.subscribe('/user/r/otherPlayed', function (game) {
      NotificationboxComponent.otherTurnGame = JSON.parse(game.body);
      _this.playedPlayer = NotificationboxComponent.otherTurnGame.players.find(p => p.orderNr === NotificationboxComponent.otherTurnGame.playerTurn - 1);
      $('#friendRequest').hide();
      $('#gameInvite').hide();
      $('#myTurn').hide();
      $('#chatMessage').hide();
      $('#otherPlayed').show();
    }, {id: 'otherPlayedNotif'});
  }

  joinGame(game: GameDto) {
    const _this = this;
    this.freshGame = new GameDto();
    this.http.get<GameDto>(this.globals.backendUrl + '/getLiteGame/' + game.id).subscribe(data => this.freshGame = data, null, function () {
      if (!(_this.freshGame.nrOfPlayers === _this.freshGame.maxPlayers)) {
        if (_this.freshGame.status === 0) {
          _this.globals.stompClient.subscribe('/r/lobby/' + game.id, function () {
            $('#gameInvite').hide();
            _this.router.navigate(['/lobby', game.id]);
          }, {id: 'lobby' + game.id});
          _this.globals.stompClient.send('/app/joinLobby', {}, JSON.stringify({'id': game.id}));
        }
        if (_this.freshGame.status === 1) {
          _this.globals.stompClient.subscribe('/r/lobby/' + _this.freshGame.id, function () {
            $('#myTurn').hide();
            $('#otherPlayed').hide();
            _this.router.navigate(['/game', _this.freshGame.id]);
          }, {id: 'lobby' + _this.freshGame.id});
          _this.globals.stompClient.send('/app/joinLobby', {}, JSON.stringify({'id': _this.freshGame.id}));
        }
        $('#chatMessage').hide();
      }
    });
  }

  subChat() {
    const _this = this;
    this.globals.stompClient.subscribe('/user/r/chatNotif', function (chatmessage) {
      NotificationboxComponent.chatMessage = JSON.parse(chatmessage.body);
      $('#myTurn').hide();
      $('#friendRequest').hide();
      $('#otherPlayed').hide();
      $('#gameInvite').hide();
      $('#chatMessage').show();
    }, {id: 'chatNotif'});
  }

  close() {
    $('#friendRequest').hide();
    $('#gameInvite').hide();
    $('#myTurn').hide();
    $('#otherPlayed').hide();
    $('#chatMessage').hide();
  }
}
