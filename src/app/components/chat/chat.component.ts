import {Component, OnInit} from '@angular/core';
import {ChatMessageDto} from '../../model/ChatMessageDto';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Globals} from '../globals';
import {GameService} from '../../services/game-service/game.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  messages: ChatMessageDto[] = [];
  message;
  messageForm;
  gameId;

  constructor(private globals: Globals, private http: HttpClient, private gameService: GameService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    const _this = this;
    this.message = new FormControl('', Validators.required);
    this.messageForm = new FormGroup({
      message: this.message
    });
    this.gameId = _this.gameService.getGame().id;
    this.globals.initializeWebSocketConnection(function () {
      _this.globals.stompClient.subscribe('/r/chat/' + _this.gameId, function (message) {
        _this.messages.push(JSON.parse(message.body));
        _this.messages = _this.messages.slice();
      }, {id: 'chat' + _this.gameId});

    });
  }

  sendMessage() {
    if (this.messageForm.valid) {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
      let messageDto: ChatMessageDto = new ChatMessageDto();
      messageDto.content = this.message.value;
      messageDto.game = this.gameService.getGame();
      this.http.post(this.globals.backendUrl + '/sendMessage', messageDto, httpOptions).subscribe();
      var inputValue = (<HTMLInputElement>document.getElementById('invoer'));
      inputValue.value = '';
    }
  }
}

