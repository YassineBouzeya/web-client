import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatComponent } from './chat.component';
import {AppModule} from '../app.module';

describe('ChatComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule]
      //declarations: [ ChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.get(ChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
    //component.
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
