import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GameDto} from '../../model/GameDto';
import {Globals} from '../globals';
import {GameService} from '../../services/game-service/game.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-host-settings',
  templateUrl: './host-settings.component.html',
  styleUrls: ['./host-settings.component.css'],
})
export class HostSettingsComponent implements OnInit {
  game: GameDto;
  settingsForm: FormGroup;
  gameName: FormControl;
  turnDuration: FormControl;
  reqBuildings: FormControl;
  maxPlayers: FormControl;
  joinType: FormControl;

  constructor(private route: ActivatedRoute, private router: Router, private globals: Globals, private gameService: GameService, private http: HttpClient) {
  }

  ngOnInit() {
    this.gameName = new FormControl('Mijn nieuw spel', Validators.required);
    this.turnDuration = new FormControl(60, [Validators.required, Validators.min(10), Validators.max(300)]);
    this.reqBuildings = new FormControl(8, [Validators.required, Validators.min(1), Validators.max(8)]);
    this.maxPlayers = new FormControl(7, [Validators.required, Validators.min(2), Validators.max(7)]);
    this.joinType = new FormControl('0', Validators.required);
    this.settingsForm = new FormGroup(
      {
        gameName: this.gameName,
        turnDuration: this.turnDuration,
        reqBuildings: this.reqBuildings,
        maxPlayers: this.maxPlayers,
        joinType: this.joinType
      }
    );
  }

  makeLobby() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const _this = this;
    if (this.settingsForm.valid) {
      this.game = new GameDto();
      this.game.gameName = _this.gameName.value;
      this.game.turnDuration = this.turnDuration.value;
      this.game.reqBuildings = this.reqBuildings.value;
      this.game.maxPlayers = this.maxPlayers.value;
      this.game.joinType = +this.joinType.value;
      this.http.post<GameDto>(this.globals.backendUrl + '/makeLobby', this.game, httpOptions).subscribe(game => this.game = game, null, function () {
        _this.globals.initializeWebSocketConnection(function () {
          _this.globals.stompClient.subscribe('/r/lobby/' + _this.game.id, function () {
            _this.router.navigate(['/lobby', _this.game.id]);
          }, {id: 'lobby' + _this.game.id});
        });
        _this.globals.stompClient.send('/app/joinLobby', {}, JSON.stringify({'id': _this.game.id}));
      });
    }
  }
}
