import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
/*import {TRANSLATIONS, TRANSLATIONS_FORMAT, LOCALE_ID} from '@angular/core';
import {Utility} from './app/services/utility.service';*/
import {AppModule} from './app/components/app.module';
import {environment} from './environments/environment';
import 'hammerjs';


if (environment.production) {
  enableProdMode();
}
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));


/*getTranslationProviders().then(providers => {
  const options = {providers};
  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));


export function getTranslationProviders(): Promise<Object[]> {
  let locale = 'fr-fr';
  const noProviders: Object[] = [];
  if (!locale || locale === 'en-US') {
    return Promise.resolve(noProviders);
  }
  const translationFile = `src/locale/messages.${locale}.xlf`;
  return getTranslationsWithImports(translationFile).then((translations: string) => [
    {provide: TRANSLATIONS, use_value: translations},
    {provide: TRANSLATIONS_FORMAT, use_value: 'xlf'},
    {provide: LOCALE_ID, use_value: locale}
  ]).catch(() => noProviders);

}

function getTranslationsWithImports(file: string) {
  const util = new Utility();
  return util.getFile(file);
}

*/
